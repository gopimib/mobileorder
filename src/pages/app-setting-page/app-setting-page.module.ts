// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// component imports section
import { AppSettingPage } from './app-setting-page';

// module imports section
import { IpsettingPageModule } from '../ipsetting/ipsetting.module';
import { LogoutPopupPageModule } from '../logout-popup/logout-popup.module';

@NgModule({
    declarations: [
        AppSettingPage,
    ],
    imports: [
        IonicPageModule.forChild(AppSettingPage),
        IpsettingPageModule,
        LogoutPopupPageModule
    ],
    exports: [
        AppSettingPage
    ]
})
export class AppSettingPageModule { }
