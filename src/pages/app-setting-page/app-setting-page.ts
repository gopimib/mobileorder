// angular imports section
import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

// ionic imports section
import { IonicPage, ModalController, Platform, AlertController } from 'ionic-angular';

// 3rd party imports section
import $ from 'jquery';

// providers imports section
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { AppServiceProvider } from '../../providers/app-service/app-service';
import { AppConfig } from '../../app/app.config';
import { Dialogs } from 'ionic-native';

@IonicPage()
@Component({
    selector: 'app-setting-page',
    templateUrl: 'app-setting-page.html'
})

export class AppSettingPage implements OnDestroy {

    public header: string = '';
    public selectedLanguage: string = '';
    public isShowLeftMenu: boolean = true;
    public showLanguagePopup: boolean = false;
    public isDoSelectClicked: boolean = false;
    public centerId: string = '';
    public selectedLanguageText: any;

    private settingSubscription: Subscription;

    constructor(private appBroadcastProvider: AppBroadcastProvider, private modalCtrl: ModalController, public platform: Platform, private dialogs: Dialogs,
        private appServiceProvider: AppServiceProvider) {

        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
        this.header = this.selectedLanguageText.CHECK_MENU_TITLES[0];
        this.settingSubscription = this.appBroadcastProvider.subscribeSettingsHeader().subscribe(message => {
            this.header = message.header;
        });
        this.centerId = localStorage.getItem('profitCenterId');
    }

    /**
     * @method showLeftmenu
     * @description used to hide right menu and show left menu
     */
    showLeftmenu() {

        $('.menu-div-right').removeClass('right-width-20');

        if ($('.menu-div-left').hasClass('left-width-20') || $('.menu-div-left').hasClass('width-10')) {
            this.appBroadcastProvider.showMenu(false, false);
        } else {
            this.appBroadcastProvider.showMenu(true, false);
        }
    }

    /**
     * @method showRightMenu
     * @description used to hide left menu and show right menu
     */
    showRightMenu() {

        $('.menu-div-left').hasClass('left-width-20');
        $('.menu-div-left').hasClass('width-10');

        if ($('.menu-div-right').hasClass('right-width-20')) {
            this.appBroadcastProvider.showMenu(false, false);
        } else {
            this.appBroadcastProvider.showMenu(false, true);
        }
        this.setImageHeight();
    }

    setImageHeight() {
        setTimeout(() => {
            let height = $('.right-menu-style .menu-content-div').height();
            let width = $('.right-menu-style .menu-content-div').width();
            $('.test').css('height', '100%');
            $('.test').css('width', '100%');
            $('.left-menu-style img').width('100%');
            $('.left-menu-style .menu-img-div').css({ 'width': '25%' });
            $('.left-menu-style .image-div').css({ 'height': '140px !important' });
        }, 300);
    }

    /**
     * @method openLanguageList
     * @description used to show or hide language list popup
     */
    openLanguageList() {
        !this.isDoSelectClicked ? this.showLanguagePopup = !this.showLanguagePopup : this.isDoSelectClicked = false;
    }

    /**
     * @method doSelectLanguage
     * @param lang selected language
     * @description used to change the langunage
     */
    doSelectLanguage(lang) {

        let modalPage = this.modalCtrl.create('LogoutPopupPage', { 'option': 'logut' });
        this.showLanguagePopup = !this.showLanguagePopup;
        this.isDoSelectClicked = true;

        if (this.selectedLanguage === lang) {
            event.preventDefault();
            return true;
        }

        modalPage.onDidDismiss(data => {

            if (data === true) {
                
                localStorage.setItem('lang', lang);
                this.appServiceProvider.setSelectedLanguageTexts('');
                let selectedLanguage = (localStorage.getItem('lang') === 'English' || localStorage.getItem('lang') === undefined || localStorage.getItem('lang') === '' || localStorage.getItem('lang') === null) ? 'english' : 'chinese';
                
                this.appServiceProvider.getLanguageBasedText(selectedLanguage).subscribe(response => {
                    this.selectedLanguage = lang;
                    this.appBroadcastProvider.showMenu(false, false);
                    this.appBroadcastProvider.publishDoLogin(false);
                    this.appBroadcastProvider.display(false);
                    this.appServiceProvider.setAddMoreItemsValues('', '');
                    this.appServiceProvider.setCheckOptionDetail('');
                    this.appServiceProvider.setMenuDetails('');
                    this.appServiceProvider.setModifersData('');
                    this.appServiceProvider.setSelectedLanguageTexts(response);
                });
            }
        });
        modalPage.present();
    }

    /**
     * @method updateProfitCenterId
     * @description used to update the profit center id
     */
    updateProfitCenterId() {
        const confirmPromise = Dialogs.prompt(this.selectedLanguageText.PROFIT_CENTER_ID_LABEL, '', [this.selectedLanguageText.UPDATE_BTN, this.selectedLanguageText.CANCEL_BTN], localStorage.getItem('profitCenterId'));
        confirmPromise.then((result) => {
            let profitCenterId = result.input1;
            let btnIndex = result.buttonIndex;
            if (btnIndex === 1) {
                localStorage.setItem('profitCenterId', profitCenterId);
                this.appBroadcastProvider.publishDoLogin(false);
            } else {
                $(".content-view").scroll();
            }
        });
    }

    ngOnDestroy() {
        this.settingSubscription.unsubscribe();
    }
}
