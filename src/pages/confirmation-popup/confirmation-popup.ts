// angular imports section
import { Component, OnInit } from '@angular/core';

// ionic imports section
import { IonicPage, NavParams, ViewController, ModalController } from 'ionic-angular';

// config imports section
import { AppConfig } from '../../app/app.config';
import { AppServiceProvider } from '../../providers/app-service/app-service';
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';

@IonicPage()
@Component({
    selector: 'page-confirmation-popup',
    templateUrl: 'confirmation-popup.html',
})

export class ConfirmationPopupPage implements OnInit{

    public inputData: any = this.navParams;
    public selectedItems: any;
    public selectedModifiers: any;
    public defaultSettings: any;
    public selectedLanguageText: any;
    public engLanguage: boolean = true;

    constructor(public navParams: NavParams, public viewCtrl: ViewController, public modalController: ModalController, public appServiceProvider: AppServiceProvider,
        public appBroadcastProvider: AppBroadcastProvider) {
        this.selectedItems = this.inputData.data.itemData;
        this.defaultSettings = this.appServiceProvider.getDefaultSettings();
        this.engLanguage = localStorage.getItem('lang') === 'English' ? true : false;
    }

    ngOnInit() {
        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
    }

    /**
     * @method doRemoveItem
     * @description used to initialize basci things after page is loaded
     */
    doRemoveItem(value) {
        this.appBroadcastProvider.publishRemoveOption(value);
    }

    /**
     * @method closePopup
     * @param value close or confirm
     * @description used to close the popup and do some action based on value parameter
     */
    closePopup(value) {

        if (value === 'close') {
            this.viewCtrl.dismiss('close');
        } else {
            this.viewCtrl.dismiss('confirm');
        }
    }

    /**
     * @method openPopup
     * @param item selected item details
     * @description used to open the modifiers popup
     */
    openPopup(item, index) {

        let data = { data: item };
        let modalPage = this.modalController.create('ImagePopupPage', data);

        modalPage.onDidDismiss(data => {

            for (let i = 0; i < this.selectedItems.length; i++) {
                if (this.selectedItems[i].type === 'Modifier') {
                    this.selectedItems.splice(i, 1);
                }
            }
            if (data.CHOICES && data.CHOICES.length) {
                for (let j = 0; j < data.CHOICES.length; j++) {
                    if (data.CHOICES[j].count > 0) {
                        for (let k = 0; k < data.CHOICES[j].count; k++) {
                            let localData = { '_ID': data.CHOICES[j].ModifierId, '_PRICE': data.CHOICES[j].ModifierPrice, '_NAME': data.CHOICES[j].ModifierName, 'type': 'Modifier' };
                            this.selectedItems.splice(index + 1, 0, localData)
                        }
                    }
                }
            }
        });
        modalPage.present();
    }
}
