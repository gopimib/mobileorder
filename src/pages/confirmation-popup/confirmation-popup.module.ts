// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// components imports section
import { ConfirmationPopupPage } from './confirmation-popup';

@NgModule({
    declarations: [
        ConfirmationPopupPage,
    ],
    imports: [
        IonicPageModule.forChild(ConfirmationPopupPage),
    ],
    exports: [
        ConfirmationPopupPage
    ]
})
export class ConfirmationPopupPageModule { }
