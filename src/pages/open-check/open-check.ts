// angular imports section
import { Component, OnDestroy } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

// ionic imports section
import { IonicPage, ModalController, ViewController } from 'ionic-angular';

// 3rd party imports section
import $ from 'jquery';
import * as _ from 'lodash';

// components imports section
import { CoverCountPage } from '../cover-count/cover-count';

// providers imports section
import { AppServiceProvider } from '../../providers/app-service/app-service';
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';

@IonicPage()
@Component({
    selector: 'page-open-check',
    templateUrl: 'open-check.html',
})
export class OpenCheckPage implements OnDestroy {

    public data: any;
    public table: any;
    public pullApiOpenOrder: any;
    public subscription: Subscription;
    public openOrdeSubscription: Subscription;
    public width: string;
    public height: string;
    public tableDetails: Array<any> = [];
    public selectedLanguageText: any;

    private myImage: SafeHtml;

    constructor(private errorHandleService: ErrorHandlerProvider, private modalController: ModalController,
        private appServiceProvider: AppServiceProvider, private appBroadcastProvider: AppBroadcastProvider, private sanitizer: DomSanitizer,
        private viewCtrl: ViewController) {

        this.subscription = this.appBroadcastProvider.subscribeMenuContentHeightInfo().subscribe(data => {
            this.setHeightAndWidth();
        });

        this.openOrdeSubscription = this.appBroadcastProvider.subscribeOpenOrdersApi().subscribe(data => {
            this.appBroadcastProvider.display(true);
            this.callopenOrderApi();
        });
        this.callopenOrderApi();
        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
    }

    /**
     * @method callopenOrderApi
     * @description used to call open orders table api
     */
    callopenOrderApi() {

        if (this.pullApiOpenOrder) {
            this.pullApiOpenOrder.unsubscribe();
        }

        this.pullApiOpenOrder = this.appServiceProvider.getOpenOrdersDetails().subscribe(openTableData => {

            if (!openTableData['error']) {

                let result = _.map(openTableData['openorderstable'], 'Table');
                let svgData = this.appServiceProvider.getFloorData();
                this.appServiceProvider.setOpenCheckDetails(result);

                if (svgData && svgData['svg'] && svgData['Height'] && svgData['Width']) {
                    this.setFloorStructure(svgData, result);
                } else {
                    if (localStorage.getItem('profitCenterId')) {
                        let screenWidth = $('.content-view').width();
                        let data = { 'profitcenterid': localStorage.getItem('profitCenterId'), 'screenWidth': screenWidth };
                        this.appServiceProvider.getFloorStructure(data).subscribe(svgData => {
                            this.setFloorStructure(svgData, result);
                        });
                    }
                }
            } else {
                this.appBroadcastProvider.display(false);
            }
        });
    }

    /**
     * @method setFloorStructure
     * @param data floor plan api response
     * @param openTableData open order table api response
     * @description used to set the svg data into content area
     */
    setFloorStructure(data, openTableData) {
        this.height = data.Height;
        this.width = data.Width;
        this.myImage = data.svg ? this.sanitizer.bypassSecurityTrustHtml(data.svg.toString()) : '';
        this.appServiceProvider.setFloorData(data);
        this.enableClickevent();
        setTimeout(() => {
            for (let i = 0; i < openTableData.length; i++) {
                $('g').filter(function () {
                    return $(this).attr('table') === openTableData[i];
                }).children('rect').css('fill', '#48474D');
            }
        }, 200);
    }

    /**
     * @method enableClickevent
     * @description used to enable click event for svg table image
     */
    enableClickevent() {
        let curThis = this;

        setTimeout(() => {
            this.setHeightAndWidth();
            $('.svgImages').click(function (event) {
                curThis.openCheck($(this).attr('table'), $(this).attr('count'));
            })
            curThis.appBroadcastProvider.setLoder(false);
        }, 1000);
    }

    /**
     * @method setHeightAndWidth
     * @description used to change height and width of the test div
     */
    setHeightAndWidth() {
        let height = $('.right-menu-style .menu-content-div').height();
        let width = $('.right-menu-style .menu-content-div').width();
        $('.test').css('height', height);
        $('.test').css('width', width);
    }

    /**
     * @method openCheck
     * @param id table number
     * @param value head count
     * @description used to show cover count popup
     */
    openCheck(id, val) {
        let value = { TABLE_ID: id, COVER_COUNT: val }
        this.openModalPopup(value);
    }

    /**
     * @method openToastBarMessage
     * @description used to show error message based on selected type
     */
    openToastBarMessage() {

        let checkOption = this.appServiceProvider.getCheckOptionDetail();

        if (checkOption.index === 0) {
            this.errorHandleService.handleError(this.selectedLanguageText.TABLE_ALREADY_BOOKED, 'info');
        } else if (checkOption.index === 2) {
            this.errorHandleService.handleError(this.selectedLanguageText.NO_DATA_AVAILABLE_FOR_THIS_TABLE, 'info');
        } else if (checkOption.index === 1) {
            this.errorHandleService.handleError(this.selectedLanguageText.RECALL_PERMISSION_ERROR, 'warning');
        }
    }

    /**
     * @method openModalPopup
     * @param value contains table number and head count
     * @description used to check selected check type and show appropriate popup
     */
    openModalPopup(value) {

        let checkOption = this.appServiceProvider.getCheckOptionDetail();
        let openChk = { 'tableId': value.TABLE_ID, 'coverCount': value.COVER_COUNT }
        let recallCheck = { 'Table': value.TABLE_ID, 'ProfitCenterId': localStorage.getItem('profitCenterId') };
        this.appBroadcastProvider.publishTableNumber(value.TABLE_ID);

        if (checkOption.index === 0) {

            this.appBroadcastProvider.setLoder(true);

            this.appServiceProvider.recallCheck(recallCheck).subscribe(data => {

                if (data['CheckNumber'] && data['CheckNumber'].toString() !== '0') {
                    this.openToastBarMessage();
                } else {
                    // if (data['CheckNumber'] !== '0') {
                    //     this.appServiceProvider.setAddMoreItemsValues(value.TABLE_ID, data['CheckNumber']);
                    // } else {
                        this.appServiceProvider.setAddMoreItemsValues('', '0');
                    // }
                    this.openModal('CoverCountPage', openChk);
                }
                this.appBroadcastProvider.setLoder(false);
            }, err => {
            });
        } else if (checkOption.index === 1) {
            this.appBroadcastProvider.setLoder(true);
            this.appServiceProvider.recallCheck(recallCheck).subscribe(data => {

                this.appBroadcastProvider.setLoder(false);
                if (data['error']) {
                    this.errorHandleService.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
                    return true;
                }
                if (data['CheckNumber'].toString() === '0') {
                    this.errorHandleService.handleError(this.selectedLanguageText.NO_CHECK_AVAILBLE_TEXT, 'info');
                } else {
                    this.appServiceProvider.setRecallData(data);
                    this.openModal('RecallCheckPopupPage', openChk);
                }
            }, err => {
                this.appBroadcastProvider.setLoder(false);
            });

        } else if (checkOption.index === 2) {

            this.appBroadcastProvider.setLoder(true);
            this.appServiceProvider.recallCheck(recallCheck).subscribe(data => {

                if (data && data['MenuItems'].length) {
                    this.appServiceProvider.setRecallData(data);
                    this.openModal('SplitCheckPage', recallCheck);
                } else {
                    this.openToastBarMessage();
                }
                this.appBroadcastProvider.setLoder(false);
            }, err => {
            });
        } else if (checkOption.index === 3) {
            this.appServiceProvider.setSelectedTableId(value.TABLE_ID);
            this.appBroadcastProvider.publishTenderCheckPage(true);
        }
    }

    /**
     * @method openModalPopup
     * @param component component name
     * @param data data to be passed to component
     * @description used to open popup based on component parameter
     */
    openModal(component, data) {
        let modalPage;
        if (modalPage) {
            this.viewCtrl.dismiss();
        }
        modalPage = this.modalController.create(component, data);
        modalPage.onDidDismiss(data => {
        });
        modalPage.present();
    }

    /**
     * @method ngOnDestroy
     * @description used to do some actions while page destroy
     */
    ngOnDestroy() {
        this.pullApiOpenOrder.unsubscribe();
        this.openOrdeSubscription.unsubscribe();
    }
}
