// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// component imports section
import { OpenCheckPage } from './open-check';

// module imports section
import { RecallCheckPopupPageModule } from '../recall-check-popup/recall-check-popup.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
    declarations: [
        OpenCheckPage,
    ],
    imports: [
        RecallCheckPopupPageModule,
        PipesModule,
        IonicPageModule.forChild(OpenCheckPage),
    ],
    exports: [
        OpenCheckPage,
    ]
})
export class OpenCheckPageModule { }
