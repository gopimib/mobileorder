// angular imports section
import { Component, OnInit } from '@angular/core';

// ionic imports section
import { IonicPage } from 'ionic-angular';

// config imports section
import { AppConfig } from '../../app/app.config';

// provider imports section
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { AppServiceProvider } from '../../providers/app-service/app-service';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})

export class LoginPage implements OnInit {

    public text: Object = {};
    public registerCredentials = { pin: '', password: '', profitCenterId: '' };
    public isShowError: boolean = false;
    public errorText: string = '';
    public languageBasedTexts: any;

    constructor(private appBroadcastProvider: AppBroadcastProvider, private appServiceProvider: AppServiceProvider) {
        this.languageBasedTexts = this.appServiceProvider.getSelectedLanguageTexts() ? this.appServiceProvider.getSelectedLanguageTexts() : '';
    }
    
    ngOnInit() {
        this.appBroadcastProvider.setSelectedLanguage.subscribe(() => {
            this.languageBasedTexts = this.appServiceProvider.getSelectedLanguageTexts();
        });
    }
    
    /**
     * @method login
     * @description ussed to check the credential and do the process
     */
    login() {
        let reqData = {};
        this.isShowError = false;

        reqData['UserName'] = this.registerCredentials.pin;
        reqData['Password'] = this.registerCredentials.password;
        reqData['ProfitCenterId'] = this.registerCredentials.profitCenterId;
        
        if (!this.registerCredentials.pin ||  !this.registerCredentials.password || !this.registerCredentials.profitCenterId) {
            this.isShowError = true;
            if (localStorage.getItem('lang') === 'Chinese') { 
                this.errorText = this.languageBasedTexts.ALL_FIELDS_MANDATORY_ERROR;
            } else {
                this.errorText = this.languageBasedTexts.ALL_FIELDS_MANDATORY_ERROR;
            }
            this.hideError();
            return true;
        }
        
        this.appBroadcastProvider.display(true);
        this.appServiceProvider.doLogin(reqData).subscribe(data => {
            if (data['Status'] === 'Valid') {
                localStorage.setItem('empId', this.registerCredentials.pin)
                localStorage.setItem('profitCenterId', this.registerCredentials.profitCenterId);
                this.callRequiredApi();
                this.appBroadcastProvider.publishDoLogin(true);
                this.appBroadcastProvider.display(false);
                let defaultSettings = {'isIconEnabled': data['IsIconEnabled'],'isImagesEnabled': data['IsImagesEnabled '], 'isSeatRequired': data['IsSeatRequired'], 'currencyType': data['CurrencyType']};
                this.appServiceProvider.setDefaultSettings(defaultSettings);
            } else if (data['Status'] === 'Invalid') {
                this.errorText = this.languageBasedTexts.INVALID_USER;
                this.isShowError = true;
                this.hideError();
                this.appBroadcastProvider.display(false);
            } else {
                this.appBroadcastProvider.display(false);
            }
        }, err => {
            this.appBroadcastProvider.display(false);
        });
    }

    hideError() {
        setTimeout(() => {
            this.isShowError = false;
        }, 1500);
    }

    /**
     * @method setModifiersData
     * @description used to get modifiers data
     */
    callRequiredApi() {
        if (!this.appServiceProvider.getModifersData()) {
            this.appServiceProvider.getModifiersList().subscribe(modifiersData => { 
                this.appServiceProvider.setModifersData(modifiersData);
            });
        }
    }
}
