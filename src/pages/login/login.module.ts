// angular imports scetion
import { NgModule } from '@angular/core';

// ionic imports scetion
import { IonicPageModule } from 'ionic-angular';

// components imports scetion
import { LoginPage } from './login';

@NgModule({
    declarations: [
        LoginPage,
    ],
    imports: [
        IonicPageModule.forChild(LoginPage),
    ],
    exports: [
        LoginPage
    ]
})
export class LoginPageModule { }
