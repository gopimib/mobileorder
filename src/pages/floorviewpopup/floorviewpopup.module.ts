import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FloorviewpopupPage } from './floorviewpopup';

@NgModule({
    declarations: [
        FloorviewpopupPage,
    ],
    imports: [
        IonicPageModule.forChild(FloorviewpopupPage),
    ],
    exports: [
        FloorviewpopupPage
    ]
})
export class FloorviewpopupPageModule { }
