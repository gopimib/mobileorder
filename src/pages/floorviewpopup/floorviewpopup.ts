// angular imports section
import { Component, OnInit } from '@angular/core';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';

// ionic imports section
import { IonicPage, ViewController, ModalController } from 'ionic-angular';

// 3rd party imports section
import * as _ from 'lodash';
import $ from 'jquery';

// providers imports section
import { AppServiceProvider } from '../../providers/app-service/app-service';
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';

@IonicPage()
@Component({
    selector: 'page-floorviewpopup',
    templateUrl: 'floorviewpopup.html',
})
export class FloorviewpopupPage implements OnInit {

    private myImage: SafeHtml;
    private tableId: string = '';

    public height: string = '';
    public width: string = '';
    public coverCount: any;
    public selectedLanguageText: any;
    public showLoader: boolean = true;

    constructor(private appServiceProvider: AppServiceProvider, private sanitizer: DomSanitizer, public viewCtrl: ViewController,
        private errorHandlerService: ErrorHandlerProvider, private appBroadcastService: AppBroadcastProvider, private modalController: ModalController) {

        this.appServiceProvider.getOpenOrdersDetails().subscribe(openTableData => {

            if (!openTableData['error']) {

                let result = _.map(openTableData['openorderstable'], 'Table');
                let svgData = this.appServiceProvider.getFloorData();
                this.appServiceProvider.setOpenCheckDetails(result);

                if (svgData && svgData['svg'] && svgData['Height'] && svgData['Width']) {
                    this.setFloorStructure(svgData, result);
                } else {
                    if (localStorage.getItem('profitCenterId')) {
                        let screenWidth = $('.content-view').width();
                        let data = { 'profitcenterid': localStorage.getItem('profitCenterId'), 'screenWidth': screenWidth };
                        this.appServiceProvider.getFloorStructure(data).subscribe(svgData => {
                            this.setFloorStructure(svgData, result);
                        });
                    }
                }
            } else {
                this.showLoader = false;
            }
        }, err => {
            this.errorHandlerService.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
        });
    }

    ngOnInit() {
        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
    }

    /**
     * @method setFloorStructure
     * @param data floor plan api response
     * @param openTableData open order table api response
     * @description used to set the svg data into content area
     */
    setFloorStructure(data, openTableData) {

        this.height = data.Height;
        this.width = data.Width;
        this.myImage = data.svg ?this.sanitizer.bypassSecurityTrustHtml(data.svg.toString()) : '';
        this.appServiceProvider.setFloorData(data);
        this.enableClickevent();

        setTimeout(() => {

            for (let i = 0; i < openTableData.length; i++) {
                $('g').filter(function () {
                    return $(this).attr('table') === openTableData[i];
                }).children('rect').css('fill', '#48474D');
            }
        }, 200);
    }

    /**
     * @method enableClickevent
     * @description used to enable click event for svg table image
     */
    enableClickevent() {
        let curThis = this;

        setTimeout(() => {
            this.setHeightAndWidth();
            $('.svgImages').click(function (event) {
                curThis.setTableId($(this).attr('table'));
                curThis.coverCount = $(this).attr('count')
                curThis.closePopup('confirm', );
            })
            this.showLoader = false;
        }, 500);
    }

    /**
     * @method setHeightAndWidth
     * @description used to change height and width of the test div
     */
    setHeightAndWidth() {

        let height = $('.floor-view .body').height();
        let width = $('.floor-view .body').width();
        $('.body .test').css('height', '100%');
        $('.body .test').css('width', '100%');
    }

    /**
     * @method closePopup
     * @param value close or confirm
     * @description used to set the table id and close the popup based on value parameter
     */
    closePopup(value) {

        this.showLoader = true;
        if (value === 'close') {
            this.viewCtrl.dismiss('close');
            this.showLoader = false;
        } else {
            let recallCheck = { 'Table': this.tableId, 'ProfitCenterId': localStorage.getItem('profitCenterId') };

            this.appServiceProvider.recallCheck(recallCheck).subscribe(data => {

                if (data && data['MenuItems'].length) {
                    this.errorHandlerService.handleError(this.selectedLanguageText.TABLE_ALREADY_BOOKED, 'info');
                } else {
                    if (data['CheckNumber'] !== '0') {
                        this.appServiceProvider.setAddMoreItemsValues(this.tableId, data['CheckNumber']);
                    } else {
                        this.appServiceProvider.setAddMoreItemsValues('', '0');
                    }
                    this.viewCtrl.dismiss('close');
                    let openChk = { 'tableId': this.tableId, 'coverCount': this.coverCount }
                    this.openModal('CoverCountPage', openChk);
                }
                this.showLoader = false;
            }, err => {
                this.errorHandlerService.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
                this.viewCtrl.dismiss('close');
            });
        }
    }

    /**
     * @method setTableId
     * @param tableId table number
     * @description used to set the table number
     */
    setTableId(tableId) {
        this.tableId = tableId;
    }

    /**
     * @method openModalPopup
     * @param component component name
     * @param data data to be passed to component
     * @description used to open popup based on component parameter
     */
    openModal(component, data) {
        let modalPage;
        if (modalPage) {
            this.viewCtrl.dismiss();
        }
        modalPage = this.modalController.create(component, data);
        modalPage.onDidDismiss(responseData => {
            this.appServiceProvider.setSelectedTableId(this.tableId);
            this.appBroadcastService.publishAddToCheckMethod();
        });
        modalPage.present();
    }
}
