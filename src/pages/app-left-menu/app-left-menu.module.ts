// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// components imports section
import { AppLeftMenuPage } from './app-left-menu';

// modules imports section
import { ImagePopupPageModule } from '../image-popup/image-popup.module';
import { ConfirmationPopupPageModule } from '../confirmation-popup/confirmation-popup.module';
import { FloorviewpopupPageModule } from '../floorviewpopup/floorviewpopup.module';


@NgModule({
    declarations: [
        AppLeftMenuPage,
    ],
    imports: [
        ImagePopupPageModule,
        ConfirmationPopupPageModule,
        FloorviewpopupPageModule,
        IonicPageModule.forChild(AppLeftMenuPage),
    ],
    exports: [
        AppLeftMenuPage
    ]
})
export class AppLeftMenuPageModule { }
