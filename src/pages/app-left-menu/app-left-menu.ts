// angular imports section
import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';

// ionic imports section
import { IonicPage, ModalController } from 'ionic-angular';

// 3rd party imports section
import * as _ from 'lodash';
import $ from 'jquery';

// components imports section
import { FloorviewpopupPage } from '../floorviewpopup/floorviewpopup';

// providers imports section
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { AppServiceProvider } from '../../providers/app-service/app-service';
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';

@IonicPage()
@Component({
    selector: 'app-left-menu',
    templateUrl: 'app-left-menu.html',
})

export class AppLeftMenuPage implements OnDestroy {

    public listItems: any = {};
    public responseData: any = {};
    public responseDataCopy: any = {};
    public listItemsDetails: Array<any> = [];
    public menuItems: Array<string> = [];
    public showMenu: Array<string> = [];
    public selectedItemWithChoice: Array<any> = [];
    public addedItemArray: Array<any> = [];
    public oldValue: Array<any> = [];
    public modifierArray: Array<any> = [];
    public rightMenuSubscription: Subscription;
    public addToCheckSubscription: Subscription;
    public toggleViewSubscription: Subscription;
    public removeItemSubscription: Subscription;
    public menuSubscription: Subscription;
    public notClose: boolean = false;
    public selectedOptionIndex: string = '0';
    public isShowImages: boolean = true;
    public selectedLanguageText: any;
    public engLanguage: boolean = true;  

    private timeoutHandler: any;
    private myImage: SafeHtml;

    constructor(public appBroadcastProvider: AppBroadcastProvider, private appServiceProvider: AppServiceProvider,
        private modalController: ModalController, private errorHandlerService: ErrorHandlerProvider, private sanitizer: DomSanitizer) {

        this.callMenuItemApi('', 'fromConstructor');

        this.engLanguage = localStorage.getItem('lang') === 'English' ? true : false;
        this.rightMenuSubscription = this.appBroadcastProvider.subscribeMenuOptionEvent().subscribe(message => {

            let localData = this.appServiceProvider.getMenuDetails();

            if (localData && localData['ITEMLIST'].ITEM.length) {
                if (message.index === -1) {
                    message.showList = this.showMenu[0];
                    message.category = this.menuItems[0];
                    message.index = 0;
                }
                this.menuItemClick(message.showList, message.category, message.index, message.fromMethod);
            } else {
                this.callMenuItemApi(message, 'fromSubscription');
            }
        });

        this.addToCheckSubscription = this.appBroadcastProvider.subscribeAddToCheckMethod().subscribe(message => {
            this.onAddToCheck();
        });

        this.appBroadcastProvider.updateItemCount.subscribe((data) => {
            this.updateItemCount(data.details);
        });

        this.toggleViewSubscription = this.appBroadcastProvider.subscribeMenuItemView().subscribe(message => {
            this.isShowImages = !message;
            this.resizeImage();
        });

        this.menuSubscription = this.appBroadcastProvider.isShowHideLeftOrRighttMenu().subscribe(message => {
            if (message.showLeft) {
                this.selectedItemWithChoice = [];
                this.resetAddedCountValues();
                this.addedItemArray = [];
            }
        });

        this.removeItemSubscription = this.appBroadcastProvider.subscribeRemoveOption().subscribe(message => {
            this.removeAddedItem(message);
        });

        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
    }

    resizeImage() {
        setTimeout(() => {
            $('.left-menu-style img').width('100%');
            $('.left-menu-style .menu-img-div').css({ 'width': '25%' });
            $('.left-menu-style .image-div').css({ 'height': '140px !important' });
        }, 500);
    }

    /**
     * @method unique
     * @param arr array of objects
     * @param prop key value of the object
     * @description used to group the array of objects bease on one key value pair
     */
    unique(arr, prop) {
        return arr.map(function (e) { return e[prop]; }).filter(function (e, i, a) {
            return i === a.indexOf(e);
        });
    }

    /**
     * @method callMenuItemApi
     * @param message subscription value
     * @param val from method
     * @description used to set menu items based on API or local data
     */
    callMenuItemApi(message, val) {

        let data = this.appServiceProvider.getMenuDetails();
        let typeLang = localStorage.getItem('lang') === 'English' ?  '_TYPE' : '_TYPE_ALT_NAME';

        if (data && data['ITEMLIST'] && data['ITEMLIST'].ITEM && data['ITEMLIST'].ITEM.length) {

            this.responseData = data['ITEMLIST'].ITEM;
            this.responseDataCopy = _.cloneDeep(data['ITEMLIST'].ITEM);

            this.showMenu = this.unique(this.responseData, typeLang);
            this.menuItems = this.unique(this.responseData, '_TYPE_ID');
            this.listItems = _.groupBy(this.responseData, '_TYPE_ID');
        } else {

            if (localStorage.getItem('profitCenterId')) {
                let data = { 'profitcenterid': localStorage.getItem('profitCenterId'), 'terminaltype': '1' };
                this.appServiceProvider.getDataAndChoice(data).subscribe(localData => {

                    if (localData && localData['ITEMLIST'] && localData['ITEMLIST'].ITEM && localData['ITEMLIST'].ITEM.length) {

                        this.responseData = localData['ITEMLIST'].ITEM;
                        this.responseDataCopy = _.cloneDeep(localData['ITEMLIST'].ITEM);
                        this.showMenu = this.unique(this.responseData, typeLang);
                        this.menuItems = this.unique(this.responseData, '_TYPE_ID');
                        this.listItems = _.groupBy(this.responseData, '_TYPE_ID');
                        this.appServiceProvider.setMenuDetails(localData);
                        if (val === 'fromSubscription') {
                            if (message.index === -1) {
                                message.category = this.menuItems[0];
                                message.showList = this.showMenu[0];
                            }
                            this.menuItemClick(message.showList, message.category, message.index, message.fromMethod);
                        }
                    }
                }, err => {
                });
            }
        }
    }

    /**
     * @method menuItemClick
     * @param category selected item type name
     * @param index selected item type index
     * @param fromMethod type of event (click or method)
     * @description used to change the item details based on the selected item type
     */
    menuItemClick(showList, category, index, fromMethod) {

        for (let i = 0; i < this.listItems.length; i++) {
            if (this.listItemsDetails[i].count > 0) {
                for (let j = 0; j < this.listItemsDetails[i].count; j++) {
                    this.selectedItemWithChoice.push(this.listItemsDetails[i]);
                }
            }
        }
        this.selectedOptionIndex = index;
        this.listItemsDetails = [];
        this.appBroadcastProvider.display(true);
        setTimeout(() => {
            if (category === undefined) {
                this.listItemsDetails = this.listItems[parseInt(this.menuItems[0])];
            } else {
                this.listItemsDetails = this.listItems[parseInt(this.menuItems[index])];
            }
            this.setImageHeight();
            this.appBroadcastProvider.display(false);
            this.setImageSource();
        }, 100);
    }

    setImageSource() {
        this.listItemsDetails.forEach(element => {
            this.appServiceProvider.getImage(element._ID).subscribe(response=> {
                element['_IMG2'] = this.sanitizer.bypassSecurityTrustUrl('data:image/png;base64, '+ response['ImageBase64String']);
            }, errorResponse => {
            });
        });
    }

    setImageHeight() {
        setTimeout(() => {
            let height = $('.right-menu-style .menu-content-div').height();
            let width = $('.right-menu-style .menu-content-div').width();
            $('.test').css('height', '100%');
            $('.test').css('width', '100%');
            $('.left-menu-style img').width('100%');
            $('.left-menu-style .menu-img-div').css({ 'width': '25%' });
            $('.left-menu-style .image-div').css({ 'height': '140px !important' });
        }, 300);
    }

    /**
     * @method openSelectedImage
     * @param imageDetails selected item details
     * @param type event type (long press or short press)
     * @description used to open the 'Add quantity popup' or 'Add modifiers popup' based on event type
     */
    openSelectedImage(imageDetails, type) {

        let choices = this.appServiceProvider.getModifersData();
        let choicesGroup = _.groupBy(choices.modifierlistresponse, 'MenuItemId');
        let matchedModifierList = choicesGroup[imageDetails._ID] ? choicesGroup[imageDetails._ID] : '';
        let modifierLang = localStorage.getItem('lang') === 'English' ?  'ModifierName' : 'ModifierAltName';

        for (let i = 0; i < matchedModifierList.length; i++) {
            matchedModifierList[i].count = 0;
            matchedModifierList[i]._NAME = matchedModifierList[i][modifierLang];
        }
        let data;
        if (type === 'shortpress') {
            data = { data: imageDetails, view: 'addQuantity' };
            this.openImagePopup(data);
        } else {
            imageDetails['CHOICES'] = matchedModifierList;
            data = { data: imageDetails, view: 'addModifiers' };
            if (imageDetails.count || imageDetails.originalCount) {
                if (matchedModifierList && matchedModifierList.length) {
                    this.openImagePopup(data);
                } else {
                    this.errorHandlerService.handleError(this.selectedLanguageText.NO_MODIFIER_ERROR, 'info');
                }
            } else {
                this.errorHandlerService.handleError(this.selectedLanguageText.ADD_ITEM_VALIDATION, 'info');
            }
        }
    }

    /**
     * @method openImagePopup
     * @param data image details
     * @description used to open image popup
     */
    openImagePopup(data) {

        let modalPage = this.modalController.create('ImagePopupPage', data);
        modalPage.onDidDismiss(localData => {

            if (localData === 'close') {
                event.preventDefault();
                return false;
            }

            if (localData === 'modifier' && this.addedItemArray.length) {
                this.addedItemArray.pop();
            }

            if (data.data.count > 0 || localData === 'modifier') {
                this.addedItemArray.push(_.cloneDeep(data.data));
                this.removeAndUpdateOrignalCount();
            }
        });
        modalPage.present();
    }

    /**
     * @method onAddToCheck
     * @description used to add procees the selected items or open the 'floor view popup' based on if condition
     */
    onAddToCheck() {

        if (this.appServiceProvider.getSelectedTableId()) {
            this.doProcess();
        } else {
            this.openSelectTablePopup();
        }
    }

    /**
     * @method doProcess
     * @description used to add the selected items in to the oldValue array, and open the 'confirmation popup ' or show the error message based on data
     */
    doProcess() {

        let data: any = {};
        this.oldValue = [];
        this.modifierArray = [];

        for (let i = 0; i < this.addedItemArray.length; i++) {
            let addedCount = this.addedItemArray[i].count ? this.addedItemArray[i].count : this.addedItemArray[i].originalCount ? this.addedItemArray[i].originalCount : 0;
            for (let j = 0; j < addedCount; j++) {
                this.addedItemArray[i]['type'] = 'Item';
                this.addedItemArray[i]['originalSeatCount'] = this.addedItemArray[i]['originalSeatCount'] ? this.addedItemArray[i]['originalSeatCount'] : this.addedItemArray[i]['seatCount'] ? this.addedItemArray[i]['seatCount'] : 1;

                if (this.addedItemArray[i]['CHOICES'] && this.addedItemArray[i]['CHOICES'].length) {
                    for (let k = 0; k < this.addedItemArray[i]['CHOICES'].length; k++) {
                        if (this.addedItemArray[i]['CHOICES'][k].count) {
                            for (let l = 0; l < this.addedItemArray[i]['CHOICES'][k].count; l++) {
                                this.addedItemArray[i]['MenuModifiers'] = [];
                                let name = localStorage.getItem('lang')  === 'English' ? this.addedItemArray[i]['CHOICES'][k].ModifierName : this.addedItemArray[i]['CHOICES'][k].ModifierAltName;
                                let data = {
                                    '_NAME': name,
                                    'type': 'Modifier',
                                    '_PRICE': parseFloat(this.addedItemArray[i]['CHOICES'][k].ModifierPrice).toFixed(2),
                                    '_ID': this.addedItemArray[i]['CHOICES'][k].ModifierId,
                                    '_ASSOCIATIONITEMID': this.addedItemArray[i]['CHOICES'][k].MenuItemId,
                                    '_SPLINSTRUCTION': this.addedItemArray[i]['CHOICES'][k].SplInstruction ? this.addedItemArray[i]['CHOICES'][k].SplInstruction : null,
                                    '_NAMEWITHPRICE': name +
                                        '(' + this.appServiceProvider.getDefaultSettings().currencyType + parseFloat(this.addedItemArray[i]['CHOICES'][k].ModifierPrice).toFixed(2) + ')'

                                };
                                this.addedItemArray[i]['_TOTALPRICE'] = parseFloat(this.addedItemArray[i]['_PRICE'] + this.addedItemArray[i]['CHOICES'][k].ModifierPrice).toFixed(2);
                                this.addedItemArray[i]['MenuModifiers'].push(data);
                            }
                        }
                    }
                }
                this.addedItemArray[i]['_TOTALPRICE'] = this.addedItemArray[i]['_TOTALPRICE'] ? this.addedItemArray[i]['_TOTALPRICE'] : parseFloat(this.addedItemArray[i]['_PRICE']).toFixed(2);
                this.oldValue.push(this.addedItemArray[i]);
            }
        }

        data = { itemData: this.oldValue };
        if (data.itemData.length) {
            this.openPopup(data);
        } else {
            this.openToastBarMessage(3);
        }
    }

    /**
     * @method openPopup
     * @param data object contains selected items and details
     * @description used to add the selected items in to the selected table
     */
    openPopup(data) {
        let modalPage = this.modalController.create('ConfirmationPopupPage', data);

        modalPage.onDidDismiss(data => {
            if (data === 'close') {
                this.notClose = true;
                for (let i = 0; i < this.selectedItemWithChoice.length; i++) {
                    let index = _.findIndex(this.oldValue, { _ID: this.selectedItemWithChoice[i]._ID });
                    if (index > -1 && this.selectedItemWithChoice[i].count) {
                        this.oldValue.splice(index, 1);
                    }
                }

                for (let i = 0; i < this.selectedItemWithChoice.length; i++) {
                    if (this.selectedItemWithChoice[i].CHOICES && this.selectedItemWithChoice[i].CHOICES.length) {
                        let index = _.findIndex(this.modifierArray, { ModifierId: this.selectedItemWithChoice[i].CHOICES.ModifierId });
                        if (index > -1 && this.selectedItemWithChoice[i].CHOICES.count) {
                            this.modifierArray.splice(index, 1);
                        }
                    }
                }
            } else {
                this.resetAddedCountValues();
                this.addedItemArray = [];
                for (let k = 0; k < this.oldValue.length; k++) {
                    this.oldValue[k]['added'] = true;
                }
                this.notClose = false;
            }

            if (data === 'confirm') {

                let requestData = [];
                
                for (let i = 0; i < this.oldValue.length; i++) {
                    
                    let data = {};
                    let name = localStorage.getItem('lang')  === 'English' ? this.oldValue[i]._NAME : this.oldValue[i]._NAME;
                    data['Id'] = this.oldValue[i]._ID;
                    data['ItemPrice'] = this.oldValue[i]._PRICE;
                    data['Quantity'] = 1;
                    data['ItemName'] = name;
                    data['seat'] = this.oldValue[i].seatCount ? this.oldValue[i].seatCount : 1;

                    if (this.oldValue[i].MenuModifiers && this.oldValue[i].MenuModifiers.length) {
                        let modifierData = {};
                        data['MenuModifiers'] = [];
                        modifierData['ModifierId'] = this.oldValue[i].MenuModifiers[0]._ID;
                        modifierData['AssociatedItemId'] = this.oldValue[i].MenuModifiers[0]._ASSOCIATIONITEMID;
                        modifierData['ModifierUnitPrice'] = this.oldValue[i].MenuModifiers[0]._PRICE;
                        modifierData['ModifierQty'] = 1;
                        modifierData['SplInstruction'] = this.oldValue[i].MenuModifiers[0]._SPLINSTRUCTION;
                        modifierData['modifierName'] = this.oldValue[i].MenuModifiers[0]._NAME;
                        data['MenuModifiers'].push(modifierData);
                    }
                    requestData.push(data);
                }

                let reqData = {
                    'MenuItems': requestData,
                    'Table': this.appServiceProvider.getSelectedTableId(),
                    'CheckType': '1',
                    'ProfitCenterId': localStorage.getItem('profitCenterId'),
                    'CoverCount': this.appServiceProvider.getHeadCountOnTable()
                };

                let data1 = this.appServiceProvider.getRecallData();
                let existingTotal: any = 0;

                if (data1 && data1['MenuItems'] && data1['MenuItems'].length) {
                    for (let i = 0; i < data1['MenuItems'].length; i++) {
                        existingTotal = existingTotal + parseInt(data1['MenuItems'][i].BillAmount);
                    }
                }

                if (this.appServiceProvider.getAddMoreItemsValues()) {
                    reqData['MenuItems'] = requestData;
                    reqData['CheckNumber'] = this.appServiceProvider.getAddMoreItemsValues().checkNumber;
                    reqData['ExistingAmt'] = existingTotal;
                    this.doModifyCheck(reqData);
                    this.appServiceProvider.setAddMoreItemsValues('', '');
                } else {
                    this.doOpenCheck(reqData);
                }
                this.appServiceProvider.setSelectedTableId('');
            }
            this.selectedItemWithChoice = [];
        });
        modalPage.present();
    }

    /**
     * @method resetAddedCountValues
     * @description used to clear added count values
     */
    resetAddedCountValues() {
        for (let i = 0; i < this.responseData.length; i++) {
            this.responseData[i]['count'] ? delete this.responseData[i]['count'] : '';
            this.responseData[i]['originalCount'] ? delete this.responseData[i]['originalCount'] : '';
            this.responseData[i]['seatCount'] ? delete this.responseData[i]['seatCount'] : '';
            if (this.responseData[i].CHOICES && this.responseData[i].CHOICES.length) {
                for (let j = 0; j < this.responseData[i].CHOICES.length; j++) {
                    this.responseData[i].CHOICES[j].count = 0;
                }
            }
        }
        for (let i = 0; i < this.listItemsDetails.length; i++) {
            this.listItemsDetails[i]['count'] ? delete this.listItemsDetails[i]['count'] : '';
        }
    }
  
    /**
     * @method removeAndUpdateOrignalCount
     * @description used to remove existin count and update original count
     */
    removeAndUpdateOrignalCount() {
        for (let i = 0; i < this.responseData.length; i++) {
            this.responseData[i]['originalCount'] = this.responseData[i]['count'] ? this.responseData[i]['count'] : this.responseData[i]['originalCount'] ? this.responseData[i]['originalCount'] : '';
            this.responseData[i]['count'] ? delete this.responseData[i]['count'] : '';
            this.responseData[i]['originalSeatCount'] = this.responseData[i]['seatCount'] ? this.responseData[i]['seatCount'] : this.responseData[i]['originalSeatCount'] ? this.responseData[i]['originalSeatCount'] : 1;
            this.responseData[i]['seatCount'] ? delete this.responseData[i]['count'] : '';
            if (this.responseData[i].CHOICES && this.responseData[i].CHOICES.length) {
                for (let j = 0; j < this.responseData[i].CHOICES.length; j++) {
                    this.responseData[i].CHOICES[j].count = 0;
                }
            }
        }
        for (let i = 0; i < this.listItemsDetails.length; i++) {
            this.listItemsDetails[i]['count'] ? delete this.listItemsDetails[i]['count'] : '';
        }
    }

    /**
     * @method doOpenCheck
     * @param reqData object contains empty items, table number, check type
     * @description used to open the new check for selected table with empty items
     */
    doOpenCheck(reqData) {
        this.appServiceProvider.doOpencheck(reqData).subscribe(data => {
            if (data && !data['error']) {
                this.openToastBarMessage(1);
                this.oldValue = [];
                this.modifierArray = [];
                this.appBroadcastProvider.showMenu(true, false);
            }
        });
    }

    /**
     * @method doModifyCheck
     * @param reqData object contains selected items, table number, check type and check number
     * @description used to add the items to the selected table
     */
    doModifyCheck(reqData) {
        this.appBroadcastProvider.setLoder(true);
        this.appServiceProvider.doModifyCheck(reqData).subscribe(data => {

            if (data && !data['error']) {
                this.appBroadcastProvider.setLoder(false);
                this.openToastBarMessage(2);
                this.oldValue = [];
                this.modifierArray = [];
                this.appBroadcastProvider.showMenu(true, false);
                for (let i = 0; i < this.responseData.length; i++) {
                    this.responseData[i].count ? delete this.responseData[i]['count'] : '';
                    this.responseData[i].seatcount ? delete this.responseData[i]['seatcount'] : '';
                }
                this.appBroadcastProvider.callOpenOrdersApi();
            }
        });
    }

    /**
     * @method openToastBarMessage
     * @param val number
     * @description used to show the error based on val parameter
     */
    openToastBarMessage(val) {

        let message;

        if (val === 1) {
            this.errorHandlerService.handleError(this.selectedLanguageText.PREAPARATION_STARTED_TEXT, 'success');
        } else if (val === 2) {
            this.errorHandlerService.handleError(this.selectedLanguageText.ORDER_UPDATED_TEXT, 'success');
        } else {
            this.errorHandlerService.handleError(this.selectedLanguageText.ADD_ITEM_VALIDATION, 'info');
        }
    }

    /**
     * @method updateItemCount
     * @param item object contains item details
     * @description used to update the count of the selected item
     */
    updateItemCount(item) {
        if (item) {
            item.count = item.count ? item.count : 0;
        }
        return false;
    }

    /**
     * @method updateSeatInfo
     * @param item object contains item details
     * @description used to update the seat number for the selected added item
     */
    updateSeatInfo(item) {
        if (item) {
            item.seatCount = item.seatCount ? item.seatCount : 0;
        }
        return false;
    }

    /**
     * @method openSelectTablePopup
     * @description used to open the floor view popup
     */
    openSelectTablePopup() {

        let modalPage;
        modalPage = this.modalController.create(FloorviewpopupPage, {});
        modalPage.onDidDismiss(data => {
            if (data === 'Availble') {
                this.onAddToCheck();
            }
        });
        modalPage.present();
    }

    /**
     * @method mouseup
     * @param item contains item detils
     * @description used to handle short press and long press event
     */
    mouseup(item) {
        if (this.timeoutHandler) {
            clearTimeout(this.timeoutHandler);
            this.timeoutHandler = null;
            this.openSelectedImage(item, 'shortpress');
        }
    }

    /**
     * @method mousedown
     * @param item contains item detils
     * @description used to handle short press and long press event
     */
    mousedown(item) {
        this.timeoutHandler = setTimeout(() => {
            this.timeoutHandler = null;
            this.openSelectedImage(item, 'longpress');
        }, 800);
    }

    /**
     * @method removeAddedItem
     * @param item contains item detils
     * @description used to remove item
     */
    removeAddedItem(item) {
        item['originalSeatCount'] = item.seatCount ? item.seatCount : item.originalSeatCount ? item.originalSeatCount : 1;
        let index: number = -1;
        let isFind: boolean = false;
        for (let i = 0; i < this.addedItemArray.length; i++) {
            if (item['MenuModifiers'] && item['MenuModifiers'].length && this.addedItemArray[i].MenuModifiers && this.addedItemArray[i].MenuModifiers.length) {
                if (this.addedItemArray[i]._ID === item._ID && this.addedItemArray[i].originalSeatCount === item.originalSeatCount && this.addedItemArray[i].MenuModifiers._ID === item.MenuModifiers._ID && !isFind) {
                    index = i;
                    isFind = true;
                }
            } else {
                if (this.addedItemArray[i]._ID === item._ID && this.addedItemArray[i].originalSeatCount === item.originalSeatCount && !isFind) {
                    index = i;
                    isFind = true;
                }
            }
        }
        this.addedItemArray.splice(index, 1);
        this.oldValue.splice(index, 1);
    }

    ngOnDestroy() {
        this.rightMenuSubscription.unsubscribe();
        this.addToCheckSubscription.unsubscribe();
    }
}

