// angular imports section
import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

// ionic imports section
import { IonicPage, LoadingController } from 'ionic-angular';

// provider imports section
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { AppServiceProvider } from '../../providers/app-service/app-service';

@IonicPage()
@Component({
    selector: 'app-right-menu',
    templateUrl: 'app-right-menu.html',
})

export class AppRightMenuPage implements OnDestroy{

    public isShowOrderStatus: boolean = false;
    public isShowOpenCheck: boolean = false;
    public isShowTenderCheck: boolean = false;
    public isShowPrintSummary: boolean = false;
    public subscription: Subscription;
    public loaderSubsription: Subscription;
    public leftMenuSubscription: Subscription;
    public selectedOpitonIdex: number = 0;

    constructor(public appBroadcastProvider: AppBroadcastProvider, private appServiceProvider: AppServiceProvider,
        private loadingController: LoadingController) {

        this.subscription = this.appBroadcastProvider.subscribeTenderCheckPage().subscribe(message => {
            this.setFalseValue();
            this.setTrueValue('tender');
        });

        this.loaderSubsription = this.appBroadcastProvider.subscribeLoderInfo().subscribe(message => {
            this.appBroadcastProvider.display(message.isShow);
        });

        this.leftMenuSubscription = this.appBroadcastProvider.subscribeCheckOptionEvent().subscribe(message => {
            message.category === 'order' || message.category === 'print'  ? this.openOtherOptions(message.category) : this.openMenuItem(message.category, message.index, message.fromMethod);
        });
    }

    /**
     * @method openOtherOptions
     * @description uesd to open the orders details page or print page
     */
    openOtherOptions(value) {
        this.setFalseValue();
        this.selectedOpitonIdex = -1;
        this.setTrueValue(value);
    }

    /**
     * @method openMenuItem
     * @param category selected check option type
     * @param index selected check option index
     * @param fromMethod calling method (click or cll by method)
     * @description uesd to set the current view
     */
    openMenuItem(category, index, fromMethod) {

        let option = { 'category': category, 'index': index };

        if (index === 0) {
            this.openNewCheck(category, index, fromMethod, option);
        } else if (index === 1) {
            this.openRecallCheck(category, index, fromMethod, option);
        } else if (index === 2) {
            this.openRecallCheck(category, index, fromMethod, option);
        } else if (index === 3) {
            this.openNewCheck(category, index, fromMethod, option);
        } else {
            this.setFalseValue();
            this.appBroadcastProvider.display(false);
            this.selectedOpitonIdex = index;
        }
    }

    /**
     * @method setTrueValue
     * @param val boolean
     * @description uesd to show or hide the check page, order status page or tender page based on val parameter
     */
    setTrueValue(val) {

        this.setFalseValue();

        setTimeout(() => {

            if (val === 'open') {
                this.isShowOpenCheck = true;
            } else if (val === 'order') {
                this.isShowOrderStatus = true;
            } else if (val === 'tender') {
                this.isShowTenderCheck = true;
            } else if (val === 'print') {
                this.isShowPrintSummary = true;
            }
        }, 200);
    }

    /**
     * @method openNewCheck
     * @param category selected check option type
     * @param index number
     * @param fromMethod calling method (click or cll by method)
     * @param option selected table details
     * @description uesd to change the view of the left menu content
     */
    openNewCheck(category, index, fromMethod, option) {

        this.appServiceProvider.setCheckOptionDetail(option);
        if (this.selectedOpitonIdex !== index && fromMethod === 'click' || this.selectedOpitonIdex === index && fromMethod === 'click' || index === 3) {
            this.appBroadcastProvider.display(true);
            this.appServiceProvider.setSelectedTableId('');
            this.setTrueValue('open');
            this.selectedOpitonIdex = index;
        }
    }

    /**
     * @method openRecallCheck
     * @param category selected check option type
     * @param index number
     * @param fromMethod calling method (click or cll by method)
     * @param option selected table details
     * @description uesd to change the view of the left menu content
     */
    openRecallCheck(category, index, fromMethod, option) {
        this.openNewCheck(category, index, fromMethod, option);
    }

    /**
     * @method setFalseValue
     * @description uesd to destroy all dom
     */
    setFalseValue() {
        this.isShowOrderStatus = false;
        this.isShowOpenCheck = false;
        this.isShowTenderCheck = false;
        this.isShowPrintSummary = false;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.loaderSubsription.unsubscribe();
        this.leftMenuSubscription.unsubscribe();
    }
}
