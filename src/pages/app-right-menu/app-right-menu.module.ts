// angular imports sectiom
import { NgModule } from '@angular/core';

// ionic imports sectiom
import { IonicPageModule } from 'ionic-angular';

// component imports sectiom
import { AppRightMenuPage } from './app-right-menu';

// module imports sectiom
import { OpenCheckPageModule } from '../open-check/open-check.module';
import { OrderDetailsPageModule } from '../order-details/order-details.module';
import { CoverCountPageModule } from '../cover-count/cover-count.module';
import { TenderCheckPageModule } from '../tender-check/tender-check.module'
import { PrintSummaryPageModule } from '../print-summary/print-summary.module';

@NgModule({
    declarations: [
        AppRightMenuPage,
    ],
    imports: [
        OpenCheckPageModule,
        OrderDetailsPageModule,
        CoverCountPageModule,
        TenderCheckPageModule,
        PrintSummaryPageModule,
        IonicPageModule.forChild(AppRightMenuPage),
    ],
    exports: [
        AppRightMenuPage
    ]
})
export class AppRightMenuPageModule { }
