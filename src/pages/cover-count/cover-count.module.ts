// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// components imports section
import { CoverCountPage } from './cover-count';

// module imports section
import { SplitCheckPageModule } from '../split-check/split-check.module';

@NgModule({
    declarations: [
        CoverCountPage,
    ],
    imports: [
        SplitCheckPageModule,
        IonicPageModule.forChild(CoverCountPage),
    ],
    exports: [
        CoverCountPage
    ]
})
export class CoverCountPageModule { }
