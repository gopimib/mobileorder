// angular imports section
import { Component } from '@angular/core';

// ionic imports section
import { IonicPage, NavParams, ViewController, ModalController } from 'ionic-angular';

// 3rd party imports section
import $ from 'jquery';

// module imports section
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { AppServiceProvider } from '../../providers/app-service/app-service';
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';

@IonicPage()
@Component({
    selector: 'page-cover-count',
    templateUrl: 'cover-count.html',
})
export class CoverCountPage {

    public checkNumber: string = '';
    public popupHeader: string = '';
    public coverCount: number = 0;
    public tableCoverCount: number = 0;
    public showLoader: boolean = false;
    public tableId: any = '';
    public selectedLanguageText: any;

    constructor(public navParams: NavParams, private viewCtrl: ViewController, private appBroadcastProvider: AppBroadcastProvider,
        private appServiceProvider: AppServiceProvider, private modalCtrl: ModalController, private errorHandlerProvider: ErrorHandlerProvider) {

        this.tableId = this.navParams;
        this.tableCoverCount = Number(this.tableId.data.coverCount);
        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
        this.popupHeader = this.selectedLanguageText.OPEN_CHECK_HEADER_TEXT + '(' + this.selectedLanguageText.TABLE_TEXT+ '-' + this.tableId.data.tableId + ' )';
    }

    /**
     * @method closePopup
     * @param val gotobtn or close
     * @description used to hide left menu and show right menu and set selected table id based on if condition atlast close the popup
     */
    closePopup(val) {
        if (val === 'gotobtn') {
            let reqData = {
                'MenuItems': [],
                'Table': this.tableId.data.tableId,
                'CheckType': '1',
                'ProfitCenterId': localStorage.getItem('profitCenterId'),
                'CoverCount': this.coverCount
            };

            this.showLoader = true;
            this.appBroadcastProvider.display(true);

            if ((this.appServiceProvider.getAddMoreItemsValues().checkNumber).toString() === '0') {
                this.appServiceProvider.doOpencheck(reqData).subscribe(data => {
                    if (data && !data['error']) {
                        this.checkNumber = data['CHECK_NUMBER'];
                        this.appServiceProvider.setAddMoreItemsValues(this.tableId.data.tableId, this.checkNumber);
                        this.appBroadcastProvider.showMenu(false, true);
                        this.appServiceProvider.setSelectedTableId(this.tableId.data.tableId);
                        this.appServiceProvider.setHeadCountOnTable(this.coverCount);
                        this.appBroadcastProvider.callOpenOrdersApi();
                    }
                    this.showLoader = false;
                    this.appBroadcastProvider.display(false);
                }, err => {
                    this.errorHandlerProvider.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
                    this.showLoader = false;
                    this.appBroadcastProvider.display(false);
                });
            } else {
                this.showLoader = false;
                this.appBroadcastProvider.display(false);
                this.appBroadcastProvider.showMenu(false, true);
                this.checkNumber = this.appServiceProvider.getAddMoreItemsValues().checkNumber;
                this.appServiceProvider.setAddMoreItemsValues(this.tableId.data.tableId, this.checkNumber);
                this.appServiceProvider.setSelectedTableId(this.tableId.data.tableId);
                this.appServiceProvider.setHeadCountOnTable(this.coverCount);
            }
        }
        this.viewCtrl.dismiss();
    }

    /**
     * @method addCoverCount
     * @description used to add the head count for the particular table
     */
    addCoverCount() {
        if (this.coverCount < this.tableCoverCount) {
            ++this.coverCount;
            $('div#count-label').text(this.coverCount);
        }
    }

    /**
     * @method removeCoverCount
     * @description used to remove the head count for the particular table
     */
    removeCoverCount() {
        if (this.coverCount > 0) {
            --this.coverCount;
            $('div#count-label').text(this.coverCount);
        }
    }
}

