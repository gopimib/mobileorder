// angular imports section
import { Component } from '@angular/core';

// ionic imports section
import { IonicPage } from 'ionic-angular';

import { AppServiceProvider } from '../../providers/app-service/app-service';
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';

@IonicPage()
@Component({
    selector: 'page-order-details',
    templateUrl: 'order-details.html',
})
export class OrderDetailsPage {

    public orderDetailsArray: any = [];
    public emptyBlock: boolean = false;
    public menuItem: any;
    public selectedLanguageText: any;

    constructor(public appServiceProvider: AppServiceProvider, public appBroadcastProvider: AppBroadcastProvider, public errorHandlerProvider: ErrorHandlerProvider) {

        let data = { 'ProfitcenterId': localStorage.getItem('profitCenterId') };
        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
        this.appServiceProvider.getOpenOrdersStatus(data).subscribe(response => {

            if (response && response['CheckDetails'] && response['CheckDetails'].length) {
                this.orderDetailsArray = response['CheckDetails'];
                this.parseResponseData();
                this.appBroadcastProvider.display(false);
            } else {
                this.emptyBlock = true;
                this.appBroadcastProvider.display(false);
            }
        }, errResponse => {
            this.errorHandlerProvider.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
        });
    }

    parseResponseData() {
        
        for (let i = 0; i < this.orderDetailsArray.length; i++) {
            let firstIndex = this.orderDetailsArray[i]['OpenTime'].indexOf(' ');
            let lastIndex = this.orderDetailsArray[i]['OpenTime'].lastIndexOf(' ');
            let subStringIndex = lastIndex - firstIndex;
            this.orderDetailsArray[i]['OpenTime'] = this.orderDetailsArray[i]['OpenTime'].substr(firstIndex, subStringIndex);
        }
        
        let menuDetails = this.appServiceProvider.getMenuDetails();
        let typeLang = localStorage.getItem('lang') === 'English' ?  '_NAME' : '_ALT_NAME';
        this.menuItem = menuDetails['ITEMLIST'].ITEM;

        for (let i = 0; i < this.orderDetailsArray.length; i++) {
            if (this.orderDetailsArray[i] && this.orderDetailsArray[i].Table && this.orderDetailsArray[i].Table.Seat) {
            } else {
                this.orderDetailsArray.splice(i, 1);
            }
        }

        for (let i = 0; i < this.orderDetailsArray.length; i++) {
            for (let j = 0; j < this.menuItem.length; j++) {
                if (this.orderDetailsArray[i].Table.Seat) {
                    for (let k = 0; k < this.orderDetailsArray[i].Table.Seat.length; k++) {
                        
                        if (this.menuItem[j]['_ID'].toString() === this.orderDetailsArray[i].Table.Seat[k].MenuItem.ID) {
                            this.orderDetailsArray[i].Table.Seat[k].MenuItem.name = this.menuItem[j][typeLang];
                            continue;
                        }
                    }
                }
            }
        }

        if (this.orderDetailsArray.length) {
            this.emptyBlock = false;
        } else {
            this.emptyBlock = true;
        }
    }
}
