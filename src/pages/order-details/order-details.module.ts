// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// components imports section
import { OrderDetailsPage } from './order-details';

@NgModule({
    declarations: [
        OrderDetailsPage,
    ],
    imports: [
        IonicPageModule.forChild(OrderDetailsPage),
    ],
    exports: [
        OrderDetailsPage
    ]
})
export class OrderDetailsPageModule { }
