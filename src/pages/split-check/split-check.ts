// angular imports section
import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/interval';

// ionic imports section
import { IonicPage, NavParams, ViewController, ModalController } from 'ionic-angular';
import { Dialogs } from 'ionic-native';

// 3rd party imports section
import $ from 'jquery';
import * as _ from 'lodash';

// providers imports section
import { AppServiceProvider } from '../../providers/app-service/app-service';
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';

@IonicPage()
@Component({
    selector: 'page-split-check',
    templateUrl: 'split-check.html',
})
export class SplitCheckPage {

    public splitNumberArray: Array<any> = [1, 2, 3, 4, 5];
    public checkDetails: Array<any> = [];
    public splitGroupArray: Array<object> = [];
    public selectedCheck: any;
    public selectedValue: any;
    public sub: any;
    public pullApiSub: any;
    public tableId: any = this.navParams.data;
    public selectedItems: any = '';
    public checkNumber: any = '';
    public hideSplitGroup: boolean = true;
    public hideSplitOptions: boolean = false;
    public qrCodeView: boolean = false;
    public stopPullApicall: boolean = false;
    public paymentSuccess: boolean = false;
    public disableBackButton: boolean = false;
    public showLoader: boolean = true;
    public splitCheckPaidCount: number = 0;
    public total: number = 0;
    public tip: number = 0;
    public tax: number = 0;
    public myIndex = 0;
    public createdCode: string = null;
    public popupHeader: string;
    public selectedLanguageText: any;
    public defaultSettings: any;

    constructor(public navParams: NavParams, private viewCtrl: ViewController,
        public appServiceProvider: AppServiceProvider, private errorHandlerService: ErrorHandlerProvider, private appBroadcastProvider: AppBroadcastProvider,
        public modalController: ModalController) {

        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
        this.popupHeader = this.selectedLanguageText.SPLIT_CHECK_HEADER_TEXT + '(' + this.selectedLanguageText.TABLE_TEXT+ '-' + this.tableId.Table + ' )';

        this.selectedItems = this.appServiceProvider.getRecallData();
        this.checkNumber = this.selectedItems['CheckNumber'];
        this.defaultSettings = this.appServiceProvider.getDefaultSettings();

        let checkNumber = { 'CheckNumber': this.checkNumber };
        this.appServiceProvider.getSplitCheck(checkNumber).subscribe(data => {
            let count = 0;
            if (data && data['splitcheckresponse'] && data['splitcheckresponse'].length) {
                for (let i = 0; i < data['splitcheckresponse'].length; i++) {
                    if (data['splitcheckresponse'][i].Status === 'completed') {
                        count = 1;
                    }
                }
                if (count) {
                    this.hideSplitGroup = false;
                    this.hideSplitOptions = true;
                    this.splitGroupArray = data['splitcheckresponse'];
                    this.disableBackButton = true;
                } else {
                    this.setRecallData();
                }
            } else {
                this.setRecallData();
            }
            this.showLoader = false;
        }, err => {
            this.showLoader = false;
        });
    }

    /**
     * @method setRecallData
     * @description used to set the data to selectedItems array and show in html
     */
    setRecallData() {
        let menuDetails = this.appServiceProvider.getMenuDetails();
        let menuItem = menuDetails['ITEMLIST'].ITEM;
        let typeLang = localStorage.getItem('lang') === 'English' ?  '_NAME' : '_ALT_NAME';

        for (let i = 0; i < this.selectedItems.MenuItems.length; i++) {
            for (let j = 0; j < menuItem.length; j++) {
                if (menuItem[j]._ID.toString() == this.selectedItems.MenuItems[i].Id) {
                    this.selectedItems.MenuItems[i].name = menuItem[j][typeLang];
                    continue;
                }
            }
            this.selectedItems.MenuItems[i].splitCheckNumber = 1;
        }
    }

    /**
     * @method closePopup
     * @param value 
     * @description used to close the popup
     */
    closePopup(value) {
        this.viewCtrl.dismiss('close');
    }

    /**
     * @method onChange
     * @param item menu item
     * @param index index number of the item
     * @description used to add or change the split check number for particular item
     */
    onChange(item, index, val) {
        this.selectedItems.MenuItems[index].splitCheckNumber = this.myIndex + 1;
        let id = '#' + index.toString();
        $(id + ' div.select-text').text(val);
    }

    /**
     * @method doSplitCheck
     * @description used to split the check based on check numbers
     */
    doSplitCheck() {
        this.splitGroupArray = [];
        let splitDetails = [];

        for (let i = 0; i < this.splitNumberArray.length; i++) {
            this.checkDetails[i] = [];
        }

        for (let j = 0; j < this.selectedItems.MenuItems.length; j++) {
            this.checkDetails[this.selectedItems.MenuItems[j].splitCheckNumber - 1].push(this.selectedItems.MenuItems[j]);
        }


        for (let k = 0; k < this.checkDetails.length; k++) {

            if (this.checkDetails[k].length) {

                let splitData = this.checkDetails[k];
                let amount: any = 0;
                let data;

                for (let l = 0; l < splitData.length; l++) {
                    amount = splitData[l].BillAmount + amount;
                }

                data = { 'checkNumber': splitData[0].splitCheckNumber, 'GuestNumber': splitData[0].splitCheckNumber, 'SplitAmount': parseFloat(amount).toFixed(2).toString() };
                this.splitGroupArray.push(data);
            }
        }

        for (let i = 0; i < this.splitGroupArray.length; i++) {
            splitDetails.push(
                {
                    'CheckNumber': this.checkNumber,
                    'GuestNumber': this.splitGroupArray[i]['checkNumber'].toString(),
                    'SplitAmount': this.splitGroupArray[i]['SplitAmount'],
                    'TipAmount': '0'
                }
            );
        }

        splitDetails = _.orderBy(splitDetails, 'GuestNumber', 'asc');

        for (let j = 0; j < splitDetails.length; j++) {
            if (!(parseInt(splitDetails[j].GuestNumber) === j + 1)) {
                this.openToastBarMessage('2');
                return false;
            }
        }

        let reqData = {
            'GuestDetails': splitDetails
        }

        this.showLoader = true;
        this.appServiceProvider.doSplitCheck(reqData).subscribe(data => {
            this.hideSplitGroup = false;
            this.hideSplitOptions = true;
            this.showLoader = false;
            this.total = this.calculateTotal();
        }, err => {
            this.showLoader = false;
        });
    }

    /**
     * @method goBack
     * @description used to show split options page
     */
    goBack() {
        if (this.qrCodeView) {
            this.hideSplitGroup = false;
            this.hideSplitOptions = true;

        } else {
            this.hideSplitGroup = true;
            this.hideSplitOptions = false;
        }
        this.qrCodeView = false;
    }

    /**
     * @method generateAlipayQrCode
     * @param item check details
     * @description used to generate QR code for checks using ALIPAY api 
     */
    generateAlipayQrCode(item) {
        let checkNumber = this.checkNumber.toString() + '-' + item.GuestNumber;
        let reqData = {
            'Body': 'InfoGenesis',
            'RequestId': checkNumber,
            'Currency': 'USD',
            'Quantity': '1',
            'TotalFee': item.SplitAmount,
            'PassbackParameters': 'Test Param',
            'SellerEmail': 'gopi.manickam@agilysis.com',
            'DeviceID': '12312321',
            'AppID': 'Com.Agilysys.EFMO'
        }
        this.paymentSuccess = false;
        this.showLoader = true;

        this.appServiceProvider.doGenerateAlipayQrCode(reqData).subscribe(data => {
            if (data) {
                this.createdCode = data['QRCode'];
                this.hideSplitGroup = true;
                this.hideSplitOptions = true;
                this.qrCodeView = true;
                this.showLoader = false;
                this.selectedCheck = item;
                this.sub = Observable.interval(5000)
                    .subscribe((val) => {
                        if (!this.paymentSuccess) {
                            this.callPullApi(item);
                        }
                    });
            } else {
                this.showLoader = false;
            }
        }, err => {
            this.showLoader = false;
        });
    }

    /**
     * @method callPullApi
     * @param item check details
     * @description used to check alipay payment response
     */
    callPullApi(item) {
        if (this.pullApiSub) {
            this.pullApiSub.unsubscribe();
        }

        if (this.createdCode) {

            let reqData = {
                'RequestID': this.checkNumber.toString() + '-' + item.checkNumber,
            }
            this.pullApiSub = this.appServiceProvider.checkPullAPI(reqData).subscribe(data => {
                if (data && data['STATUS'] === 'SUCCESS') {
                    this.paymentSuccess = true;
                    this.createdCode = null;
                    this.doSplitPayment(item, 'Alipay');
                } else {
                    this.showLoader = false;
                }
            }, err => {
                this.showLoader = false;
            });
        }
    }

    /**
     * @method payByCash
     * @param item check details
     * @description used to call doSplitPayment method
     */
    payByCash(item) {
        this.doSplitPayment(item, 'Cash');
    }

    /**
     * @method doSplitPayment
     * @param item check details
     * @param paymentMode cash or alipay
     * @description used to close split checks individually
     */
    doSplitPayment(item, paymentMode) {

        this.createdCode = null;
        this.qrCodeView = false;
        this.hideSplitGroup = false;
        this.hideSplitOptions = true;
        this.showLoader = true;
        this.splitCheckPaidCount = 0;

        let paymentStatus = [];
        paymentStatus.push({ 'CheckNumber': this.checkNumber, 'GuestNumber': item.GuestNumber, 'PaymentMode': paymentMode, 'Status': 'completed' });
        let reqData = {
            'PaymentStatus': paymentStatus
        };

        this.appServiceProvider.closeSplitCheck(reqData).subscribe(data => {

            this.showLoader = false;
            this.disableBackButton = true;
            item['Status'] = 'completed';

            for (let i = 0; i < this.splitGroupArray.length; i++) {
                if (this.splitGroupArray[i]['Status'] === 'completed') {
                    this.splitCheckPaidCount++;
                }
            }

            if (this.splitCheckPaidCount === this.splitGroupArray.length) {
                this.callCloseApi();
            }
        }, err => {
            this.showLoader = false;
        });
    }

    /**
     * @method calculateTotal
     * @description used to calculate total based on split checks
     */
    calculateTotal() {
        let total = 0;
        for (let i = 0; i < this.splitGroupArray.length; i++) {
            total = total + parseFloat(this.splitGroupArray[i]['SplitAmount']);
        }
        return total;
    }

    /**
     * @method callCloseApi
     * @description used to close the check
     */
    callCloseApi() {

        let total = this.calculateTotal();
        let reqData = {
            'Table': this.tableId.Table,
            'CheckType': '1',
            'CheckNumber': this.checkNumber,
            'TenderId': '1',
            'TenderAmount': parseFloat(total  + this.selectedItems.TaxAmount).toFixed(2),
            'Tip': '0',
            'ProfitCenterId': localStorage.getItem('profitCenterId')
        };
        this.showLoader = true;
        this.appServiceProvider.closeCheck(reqData).subscribe(data => {
            if (data['error']) {
                return true;
            }
            let modalPage = this.modalController.create('PinCheckPage', { 'option': 'print', 'printReceipt': data['RECEIPT_TEXT'] });
            modalPage.onDidDismiss(modalData => {

                if (modalData.type === 'email') {

                    const confirmPromise = Dialogs.prompt(this.selectedLanguageText.EMAIL_LABEL, '', [this.selectedLanguageText.SEND_BTN, this.selectedLanguageText.CANCEL_BTN], '');
                    confirmPromise.then((result) => {
                        let emailId = result.input1;
                        let btnIndex = result.buttonIndex;

                        if (btnIndex === 1) {
                            
                            if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailId)) {

                                let emailReqData = {
                                    'ToAddress': emailId,
                                    'CheckNumber': reqData['CheckNumber'],
                                    'CheckAmount': this.defaultSettings['currencyType'] + parseFloat(reqData['TenderAmount']).toFixed(2).toString(),
                                    'ReceiptText': data['RECEIPT_TEXT']
                                };
                                this.appBroadcastProvider.display(true);
                                this.appServiceProvider.sendEmail(emailReqData).subscribe(emailDataResponse => {
                                    this.appBroadcastProvider.display(false);
                                    this.errorHandlerService.handleError(this.selectedLanguageText.MAIL_SENT_TEXT, 'success');
                                },errResponse => {
                                    this.appBroadcastProvider.display(false);
                                    this.errorHandlerService.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
                                });
                            } else {
                                this.errorHandlerService.handleError(this.selectedLanguageText.INAVLID_MAIL, 'error');
                            }
                        }
                    });
                } else if (modalData.type === 'print') {
                    let printReqData = {
                        'ProfitCenterID': localStorage.getItem('profitCenterId'),
                        'EmployeeID': localStorage.getItem('empId'),
                        'PrintInfo': data['RECEIPT_TEXT']
                    };

                    this.appBroadcastProvider.display(true);
                    this.appServiceProvider.callPrintApi(printReqData).subscribe(printDataResponse => {
                        this.appBroadcastProvider.display(false);
                        this.errorHandlerService.handleError(this.selectedLanguageText.SUCCESSFULLY_PRINTED, 'success');
                    },errResponse => {
                        this.appBroadcastProvider.display(false);
                        this.errorHandlerService.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
                    });
                }
                this.appServiceProvider.setSelectedTableId('');
            });
            modalPage.present();

            if (!data['error']) {
                this.createdCode = null;
                this.appServiceProvider.setSelectedTableId('');
                this.viewCtrl.dismiss();
                this.openToastBarMessage('1');
                this.appBroadcastProvider.callOpenOrdersApi();
            }
            this.showLoader = false;
        }, err => {
        });
    }

    /**
     * @method openToastBarMessage
     * @param val 
     * @description used to handle the error
     */
    openToastBarMessage(val) {

        let message;
        if (val === '1') {
            this.errorHandlerService.handleError(this.selectedLanguageText.PAYMENT_SUCCESS, 'success');
        } else {
            this.errorHandlerService.handleError(this.selectedLanguageText.SPLIT_CHECK_SEQUENCE, 'warning');
        }
    }

    /**
     * @method ngOnDestroy
     * @description used to do some actions while page destroy
     */
    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }
}
