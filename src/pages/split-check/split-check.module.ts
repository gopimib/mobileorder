import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NgxQRCodeModule } from 'ngx-qrcode2';

import { SplitCheckPage } from './split-check';
import { PinCheckPageModule } from '../pin-check/pin-check.module';

@NgModule({
    declarations: [
        SplitCheckPage,
    ],
    imports: [
        IonicPageModule.forChild(SplitCheckPage),
        NgxQRCodeModule,
        PinCheckPageModule
    ],
    exports: [
        SplitCheckPage
    ]
})
export class SplitCheckPageModule { }
