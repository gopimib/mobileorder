// angular imports section
import { Component } from '@angular/core';

// ionic imports section
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

// 3rd party imports section
import $ from 'jquery';
import * as _ from 'lodash';

// providers imports section
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { AppServiceProvider } from '../../providers/app-service/app-service';
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';
import { Dialogs } from 'ionic-native';

@IonicPage()
@Component({
    selector: 'page-image-popup',
    templateUrl: 'image-popup.html',
})
export class ImagePopupPage {

    public itemDesc: any = [];
    public imageDetails: any = this.navParams.get('data');
    public imgDetailsCopy: any = _.cloneDeep(this.navParams.get('data'));
    public choicesList: Array<any> = this.imageDetails.CHOICES ? this.imageDetails.CHOICES : '';
    public isShowEmptyDetails: boolean = false;
    public isShowItemDetails: boolean = false;
    public isShowCountPage: boolean = true;
    public currentValueChanged: string = 'false';
    public popupHeader: string;
    public currentView: string = this.navParams.get('view');
    public currentCount: number = 0;
    public seatCount: number = 1;
    public selectedIndex: number = -1;
    public defaultSettings: any;
    public addedCoverCount: any;
    public selectedLanguageText: any;
    public modifierSelected: boolean = false;
    public isShowSeatOption: boolean;
    public engLanguage: boolean = true;

    constructor(public navParams: NavParams, public viewCtrl: ViewController, public appBroadcastProvider: AppBroadcastProvider, public appServiceProvider: AppServiceProvider,
        public errorHandlerProvider: ErrorHandlerProvider) {

        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
        if (this.currentView === 'addQuantity') {
            this.goBack();
        } else {
            this.goToModifier();
        }
        this.isShowSeatOption = this.appServiceProvider.getDefaultSettings().isSeatRequired === 'YES' ? true : false;
        this.currentCount = this.imgDetailsCopy.count ? this.imgDetailsCopy.count : 0;
        this.defaultSettings = this.appServiceProvider.getDefaultSettings();
        setTimeout(() => {
            $('p#item-cout-label').text(this.currentCount);
            $('p#seat-cout-label').text(1);
        }, 100);

        this.engLanguage = localStorage.getItem('lang') === 'English' ? true : false;
    }

    /**
     * @method closePopup
     * @param val modify or close
     * @description used to update the item count and close the popup based on val parameter
     */
    closePopup(val) {
        if (this.currentView === 'addQuantity' && val === 'qty') {
            this.appBroadcastProvider.updateItemCountValue(this.imageDetails);
        } else {
            this.imageDetails.count = this.currentCount;
        }
        this.modifierSelected = false;
        this.viewCtrl.dismiss(val);
    }

    /**
     * @method addCurrentChoice
     * @param value modifier details
     * @param index index number
     * @description used to add the modifier count
     */
    addCurrentChoice(value, index) {
        value.count++;
        $('div.modifier-text' + index).text(value.count);
    }

    /**
     * @method removeCurrentChoice
     * @param value modifier details
     * @param index index number
     * @description used to reduce the modifier count
     */
    removeCurrentChoice(value, index) {
        if (value.count !== 0) {
            value.count--;
            $('div.modifier-text' + index).text(value.count);
        }
    }

    selectRadio(value, index) {
        this.selectedIndex = index;
        value.count++;
        this.modifierSelected = true;
        this.imageDetails.CHOICES[index].count = 1;
    }

    /**
     * @method addCurrentChoice
     * @description used to add the modifier count
     */
    addSeatNumber() {
        this.imageDetails.seatCount = this.imageDetails.seatCount ? this.imageDetails.seatCount : 1;
        this.addedCoverCount = this.appServiceProvider.getHeadCountOnTable();
        
        if (this.addedCoverCount > this.imageDetails.seatCount) {
            this.imageDetails.seatCount++;
            $('p#seat-cout-label').text(this.imageDetails.seatCount);
        }
    }

    /**
     * @method removeCurrentChoice
     * @description used to reduce the modifier count
     */
    removeSeatNumber() {
        this.imageDetails.seatCount = this.imageDetails.seatCount ? this.imageDetails.seatCount : 1;
        if (this.imageDetails.seatCount > 1) {
            this.imageDetails.seatCount--;
            $('p#seat-cout-label').text(this.imageDetails.seatCount);
        }
    }

    /**
     * @method goBack
     * @description used to change the current view as item count page
     */
    goBack() {
        this.isShowCountPage = true;
        this.isShowEmptyDetails = false;
        this.isShowItemDetails = false;
        this.popupHeader = this.selectedLanguageText.ITEM_DETAILS_HEADER_TEXT;
    }

    /**
     * @method goToModifier
     * @description used to change the current view as modifiers page
     */
    goToModifier() {

        this.isShowCountPage = false;

        if (this.choicesList && this.choicesList.length) {
            this.isShowEmptyDetails = false;
            this.isShowItemDetails = true;
        } else {
            this.isShowEmptyDetails = true;
            this.isShowItemDetails = false;
        }
        this.popupHeader = this.selectedLanguageText.MODIFIER_HEADER_TEXT;
    }

    /**
     * @method addItemCount
     * @description used to add the item count
     */
    addItemCount() {
        this.imageDetails.count = this.imageDetails.count ? this.imageDetails.count : 0;
        this.imageDetails.count++;
        $('p#item-cout-label').text(this.imageDetails.count);
    }

    /**
     * @method removeItemCount
     * @description used to reduce the item count
     */
    removeItemCount() {
        this.imageDetails.count = this.imageDetails.count ? this.imageDetails.count : 0;
        if (this.imageDetails.count !== 0) {
            this.imageDetails.count = this.imageDetails.count - 1;
            $('p#item-cout-label').text(this.imageDetails.count);
        }
    }

    /**
     * @method addSpecialInstruction
     * @param value - selected modifiers details i - index
     * @description used to add special instructions
     */
    addSpecialInstruction(value, index) {
        const confirmPromise = Dialogs.prompt(this.selectedLanguageText.SPECIAL_INSTRUCTION_LABEL, '', [this.selectedLanguageText.OK_BTN, this.selectedLanguageText.CANCEL_BTN], '');
        confirmPromise.then((result) => {
            let info = result.input1;
            let btnIndex = result.buttonIndex;

            if (btnIndex === 1) {
                value.specialInstructions = info;
            }
        });
    }
}
