// angular import section
import { Component } from '@angular/core';

// ionic import section
import { IonicPage, NavParams, ViewController, ModalController } from 'ionic-angular';

import * as _ from 'lodash';

// config import section
import { AppConfig } from '../../app/app.config';

// providers import section
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { AppServiceProvider } from '../../providers/app-service/app-service';
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';
import { Dialogs } from 'ionic-native';

@IonicPage()
@Component({
    selector: 'page-recall-check-popup',
    templateUrl: 'recall-check-popup.html',
})
export class RecallCheckPopupPage {

    public tableNumber = this.navParams;
    public selectedItems: any = '';
    public itemsandModifiersArray: Array<any> = [];
    public checkNumber: any = '';
    public popupHeader: string;
    public selectedLanguageText: any;
    public okBtnText: string = AppConfig.RECALL_POPUP.RECALL_POPUP_OK;
    public closeBtnText: string = AppConfig.RECALL_POPUP.RECALL_POPUP_CANCEL;
    public defaultSettings: any;
    public isShowEmptyBlock: boolean = false;
    public isShowItemBlock: boolean = false;
    public isShowLock: boolean = false;
    public isShowUnLock: boolean = true;
    public showLoader: boolean = true;
    public isCheckAvailable: boolean = false;
    public isCheckSplited: boolean = false;
    public menuItem: Array<object>;
    public modifierData: Array<object>;
    public combinedList: Array<any> = [];


    constructor(public navParams: NavParams, private viewCtrl: ViewController,
        private appBroadcastProvider: AppBroadcastProvider, private appServiceProvider: AppServiceProvider, public modalCtrl: ModalController, public errorHandlerProvider: ErrorHandlerProvider) {

        let data = this.appServiceProvider.getRecallData();
        let typeLang = localStorage.getItem('lang') === 'English' ?  '_NAME' : '_ALT_NAME';
        let modifierLang = localStorage.getItem('lang') === 'English' ?  'ModifierName' : 'ModifierAltName';
        this.defaultSettings = this.appServiceProvider.getDefaultSettings();
        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
        this.popupHeader = this.selectedLanguageText.RECALL_CHECK_HEADER_TEXT + '(' + this.selectedLanguageText.TABLE_TEXT+ '-' + this.tableNumber.data.tableId + ' )';

        if (data && data['MenuItems'].length && !data['error']) {
            this.selectedItems = data;
            this.checkNumber = data['CheckNumber'];
            let menuDetails = this.appServiceProvider.getMenuDetails();
            this.menuItem = menuDetails['ITEMLIST'].ITEM;
            let modifierDetails = this.appServiceProvider.getModifersData();
            this.modifierData = modifierDetails['modifierlistresponse'];
            let addedModifier = this.selectedItems.MenuModifiers;

            for (let i = 0; i < this.selectedItems.MenuItems.length; i++) {
                for (let j = 0; j < this.menuItem.length; j++) {
                    if (this.menuItem[j]['_ID'].toString() === this.selectedItems.MenuItems[i].Id.toString()) {
                        this.selectedItems.MenuItems[i].name = this.menuItem[j][typeLang];
                        this.selectedItems.MenuItems[i].type = 'Item';
                        this.selectedItems.MenuItems[i].BillAmount = parseFloat(this.selectedItems.MenuItems[i].BillAmount).toFixed(2);
                        continue;
                    }
                }
            }

            if (this.modifierData) {
                for (let i = 0; i < this.selectedItems.MenuModifiers.length; i++) {
                    for (let j = 0; j < this.modifierData.length; j++) {
                        if (this.modifierData[j]['ModifierId'].toString() === this.selectedItems.MenuModifiers[i].ModifierId.toString()) {
                            this.selectedItems.MenuModifiers[i].BillAmount = this.modifierData[j]['ModifierPrice'];
                            this.selectedItems.MenuModifiers[i].Id = this.modifierData[j]['ModifierId'];
                            this.selectedItems.MenuModifiers[i].Quantity = '1';
                            this.selectedItems.MenuModifiers[i].type = 'Modifier';
                            this.selectedItems.MenuModifiers[i].name = this.modifierData[j][modifierLang] + '(' + this.defaultSettings.currencyType + parseFloat(this.selectedItems.MenuModifiers[i].BillAmount).toFixed(2)+')';
                            continue;
                        }
                    }
                }
            }


            for (let i = 0; i < this.selectedItems.MenuItems.length; i++) {
                
                this.itemsandModifiersArray.push(this.selectedItems.MenuItems[i]);
                for (let j = 0; j < this.selectedItems.MenuModifiers.length; j++) {
                    let numberOfOccurenceInModifierArray = addedModifier.reduce(function (n, modifier) {
                        return n + (modifier.ModifierId === addedModifier[j].ModifierId);
                    }, 0);

                    let numberOfOccurenceInCombinedArray = this.itemsandModifiersArray.reduce(function (n, modifier) {
                        return n + (modifier.ModifierId === addedModifier[j].ModifierId);
                    }, 0);

                    let index = _.findIndex(this.itemsandModifiersArray, { ModifierId: this.selectedItems.MenuModifiers[j].ModifierId });
                    let originalPrice = _.find(this.menuItem, { '_ID': parseInt(this.selectedItems.MenuItems[i].Id) ||  this.selectedItems.MenuItems[i].Id.toString()});
                    if (this.selectedItems.MenuModifiers[j].AssociatedItemId === this.selectedItems.MenuItems[i].Id && (index === -1 || numberOfOccurenceInCombinedArray < numberOfOccurenceInModifierArray) &&
                        parseFloat(originalPrice._PRICE) === parseFloat(this.selectedItems.MenuItems[i].BillAmount) - parseFloat(this.selectedItems.MenuModifiers[j].BillAmount) ) {
                        this.itemsandModifiersArray.push(this.selectedItems.MenuModifiers[j]);
                        j = this.selectedItems.MenuModifiers.length;
                        continue;
                    }
                }
            }

            this.isShowEmptyBlock = false;
            this.isShowItemBlock = true;
        } else {
            if (data['CheckNumber'] !== '0') {
                this.isCheckAvailable = true;
                this.checkNumber = data['CheckNumber'];
                this.selectedItems = data;
            } else {
                this.isCheckAvailable = false;
            }
            this.isShowEmptyBlock = true;
            this.isShowItemBlock = false;
        }
        this.showLoader = false;
    }

    /**
     * @method closePopup
     * @param val 
     * @description used to close the popup and do some action based on val parameter
     */
    closePopup(val) {

        if (val === 'addmore') {
            this.showLoader = true;
            let data = { 'CheckNumber': this.checkNumber }
            this.appServiceProvider.getSplitCheck(data).subscribe(data => {
                let count = 0;
                if (data && data['splitcheckresponse'] && data['splitcheckresponse'].length) {
                    for (let i = 0; i < data['splitcheckresponse'].length; i++) {
                        if (data['splitcheckresponse'][i].Status === 'completed') {
                            count = 1;
                        }
                    }
                    if (count) {
                        this.errorHandlerProvider.handleError(this.selectedLanguageText.CHECK_ALREADY_SPLITTED, 'info');
                        this.isCheckSplited = true;
                    } else {
                        this.addMoreItems();
                    }
                } else {
                    this.addMoreItems();
                }
                this.appBroadcastProvider.display(false);
            }, err => {
                this.errorHandlerProvider.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
                this.appBroadcastProvider.display(false);
            });
        } else {
            this.doClosePopup();
        }
    }

    /**
     * @method doRemoveItem
     * @description used to remove item from check
     */
    doRemoveItem(value, index) {

        if (this.isShowLock) {
            return true;
        }

        let modalPage = this.modalCtrl.create('LogoutPopupPage', { 'option': 'removeItem' });
        let data = {};
        data['Id'] = value.Id;
        data['ItemPrice'] = (value.BillAmount * -1);
        data['Quantity'] = 1;
        let menuData = [];
        menuData.push(data);
        let reqData = {
            'MenuItems': menuData,
            'Table': this.tableNumber.data.tableId,
            'CheckType': '1',
            'ProfitCenterId': localStorage.getItem('profitCenterId')
        };

        reqData['CheckNumber'] = this.appServiceProvider.getRecallData().CheckNumber;

        modalPage.onDidDismiss(data => {
            if (data === true) {
                this.selectedItems.MenuItems[index].deleted = true;
                this.appServiceProvider.doModifyCheck(reqData).subscribe(data => {
                }, err => {
                });
            }
        });
        modalPage.present();
    }

    /**
     * @method addMoreItems
     * @description used to change the view of the page and store the current selected table id and check number
     */
    addMoreItems() {

        this.isCheckSplited = false;
        this.appBroadcastProvider.showMenu(false, true);
        this.appBroadcastProvider.publishMenuOptionEvent('', -1, 'click');
        this.appServiceProvider.setAddMoreItemsValues(this.tableNumber.data.tableId, this.checkNumber);
        this.appServiceProvider.setSelectedTableId(this.tableNumber.data.tableId);
        this.appServiceProvider.setHeadCountOnTable(this.selectedItems.CoverCount);
        this.doClosePopup();
    }

    /**
     * @method doClosePopup
     * @description used to close popup
     */
    doClosePopup() {
        if (this.isShowUnLock) {
            this.viewCtrl.dismiss();
        }
    }

    /**
     * @method lockScreen
     * @description used to disable all button events
     */
    lockScreen() {
        this.isShowUnLock = false;
        this.isShowLock = true;
    }

    /**
     * @method unLockScreen
     * @description used to enable all button events
     */
    unLockScreen() {

        var confirmPromise = Dialogs.prompt(this.selectedLanguageText.PIN_VALIDATION_LABEL, '', [this.selectedLanguageText.OK_BTN, this.selectedLanguageText.CANCEL_BTN], '');
        confirmPromise.then((result) => {
            let input = result.input1;
            let btnIndex = result.buttonIndex;
            if (btnIndex === 1) {
                if (input === '1234') {
                    this.isShowUnLock = true;
                    this.isShowLock = false
                } else {
                    this.errorHandlerProvider.handleError(this.selectedLanguageText.INVALID_PIN, 'error');
                }
            }
        });
    }
}
