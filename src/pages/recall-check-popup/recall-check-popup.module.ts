// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// module imports section
import { RecallCheckPopupPage } from './recall-check-popup';
import { PinCheckPageModule } from '../pin-check/pin-check.module';

@NgModule({
    declarations: [
        RecallCheckPopupPage,
    ],
    imports: [
        PinCheckPageModule,
        IonicPageModule.forChild(RecallCheckPopupPage),
    ],
    exports: [
        RecallCheckPopupPage
    ]
})
export class RecallCheckPopupPageModule { }
