import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ErrorMessageSnackBarPage } from './error-message-snack-bar';

@NgModule({
    declarations: [
        ErrorMessageSnackBarPage,
    ],
    imports: [
        IonicPageModule.forChild(ErrorMessageSnackBarPage),
    ],
})
export class ErrorMessageSnackBarPageModule { }
