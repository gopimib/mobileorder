import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-error-message-snack-bar',
    templateUrl: 'error-message-snack-bar.html',
})
export class ErrorMessageSnackBarPage {

    public data = this.navParams;
    public errorMessage: string;
    public class: string;

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewController: ViewController) {
        this.errorMessage = this.data.data['errorMessage'];
        this.class = this.data.data['class'];
        setTimeout(() => {
            this.closePopup();
        }, 2000);
    }

    closePopup() {
        this.viewController.dismiss();
    }
}
