// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// components imports section
import { AppLandingPage } from './app-landing-page';

@NgModule({
    declarations: [
        AppLandingPage,
    ],
    imports: [
        IonicPageModule.forChild(AppLandingPage),
    ],
    exports: [
        AppLandingPage
    ]
})
export class AppLandingPageModule { }
