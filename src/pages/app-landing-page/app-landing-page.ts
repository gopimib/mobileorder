// angular imports section
import { Component } from '@angular/core';

// ionic imports section
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'app-landing-page',
    templateUrl: 'app-landing-page.html',
})
export class AppLandingPage {
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
}

