// angular imports section
import { Component } from '@angular/core';

// ionic imports section
import { IonicPage, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-ipsetting',
    templateUrl: 'ipsetting.html',
})
export class IpsettingPage {
    
    public ipnumber: string = '';
    public isRotateMainContent: boolean = false;
    public confirmationView: boolean = false;
    public ipnumberView: boolean = true;

    constructor(private viewCtrl: ViewController) {
        this.ipnumberView = true;
        this.confirmationView = false;
    }

    /**
     * @method closePopup
     * @param val ok or close
     * @description used to change the ip address and close the popup based on val parameter
     */
    closePopup(val) {
        let value = '';
        if(val === 'ok') {
            value = this.ipnumber;
        }
        this.viewCtrl.dismiss(value);
    }

    /**
     * @method changeView
     * @description used to change the view of the popup
     */
    changeView() {
        this.ipnumberView = false;
        this.confirmationView = true;
    }

    /**
     * @method onFocus
     * @description used to add the rotate css
     */
    onFocus() {
        this.isRotateMainContent = true;
    }

    /**
     * @method onBlur
     * @description used to remove the rotate css
     */
    onBlur() {
        this.isRotateMainContent = false;
    }
}
