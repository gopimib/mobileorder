// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// components imports section
import { IpsettingPage } from './ipsetting';

@NgModule({
    declarations: [
        IpsettingPage,
    ],
    imports: [
        IonicPageModule.forChild(IpsettingPage),
    ],
    exports: [
        IpsettingPage
    ]
})
export class IpsettingPageModule { }
