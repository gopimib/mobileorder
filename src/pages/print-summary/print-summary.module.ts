import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrintSummaryPage } from './print-summary';
import { PinCheckPageModule } from '../pin-check/pin-check.module';

@NgModule({
    declarations: [
        PrintSummaryPage,
    ],
    imports: [
        IonicPageModule.forChild(PrintSummaryPage),
        PinCheckPageModule
    ],
    exports: [
        PrintSummaryPage
    ]
})
export class PrintSummaryPageModule { }
