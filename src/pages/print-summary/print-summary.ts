import { Component } from '@angular/core';

import { IonicPage } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { Dialogs } from 'ionic-native';

import $ from 'jquery';

import { AppServiceProvider } from '../../providers/app-service/app-service';
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';

@IonicPage()
@Component({
    selector: 'page-print-summary',
    templateUrl: 'print-summary.html',
})
export class PrintSummaryPage {

    public printSummaryResultArray: any;
    public defaultSettings: any;
    public selectedLanguageText: any;

    constructor(public appServiceProvider: AppServiceProvider, public appBroadcastProvider: AppBroadcastProvider, public errorHandlerProvider: ErrorHandlerProvider,
        public modalController: ModalController) {
        let reqData = {
            'profitCenterId': localStorage.getItem('profitCenterId'),
            'empId': localStorage.getItem('empId').toString(),
            'noOfChecks': '6'
        }
        this.defaultSettings = this.appServiceProvider.getDefaultSettings();
        
        this.appServiceProvider.getRecentClosedChecks(reqData).subscribe(data => {
            if (data) {
                this.printSummaryResultArray = data;
                this.parseAmountValue();
            }
            this.hideSpinner();
        }, err => {
            this.hideSpinner();
            this.errorHandlerProvider.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error')
        });

        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
    }

    /**
     * @method parseAmountValue
     * @description used to parse the total amount (inter to string)
     */
    parseAmountValue() {
        for (let i = 0; i < this.printSummaryResultArray.length; i++) {
            this.printSummaryResultArray[i]['CheckAmount'] = parseFloat(this.printSummaryResultArray[i]['CheckAmount']).toFixed(2).toString();
        }
    }

    hideSpinner() {
        setTimeout(() => {
            this.appBroadcastProvider.display(false);
        }, 1000);
    }

    /**
     * @method openPrintPreview
     * @param item selected check number
     * @description used to open print preview for selected check
     */
    openPrintPreview(item) {

        let modalPage = this.modalController.create('PinCheckPage', { 'option': 'print', 'printReceipt': item['PrintReceipt'] });
        modalPage.onDidDismiss(data => {

            if (data.type === 'email') {

                const confirmPromise = Dialogs.prompt(this.selectedLanguageText.EMAIL_LABEL, '', [this.selectedLanguageText.SEND_BTN, this.selectedLanguageText.CANCEL_BTN], '');
                confirmPromise.then((result) => {
                    let emailId = result.input1;
                    let btnIndex = result.buttonIndex;

                    if (btnIndex === 1) {

                        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailId)) {

                            let emailReqData = {
                                'ToAddress': emailId,
                                'CheckNumber': item['CheckNo'],
                                'CheckAmount': this.defaultSettings['currencyType'] + item['CheckAmount'],
                                'ReceiptText': item['PrintReceipt']
                            };
                            this.appBroadcastProvider.display(true);
                            this.appServiceProvider.sendEmail(emailReqData).subscribe(emailDataResponse => {
                                this.appBroadcastProvider.display(false);
                                this.errorHandlerProvider.handleError(this.selectedLanguageText.MAIL_SENT_TEXT, 'success');
                            },errResponse => {
                                this.appBroadcastProvider.display(false);
                                this.errorHandlerProvider.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
                            });
                            $(".content-view" ).scroll();
                        } else {
                            this.errorHandlerProvider.handleError(this.selectedLanguageText.INAVLID_MAIL, 'error');
                            $(".content-view" ).scroll();
                        }
                    } else {
                        $(".content-view" ).scroll();
                    }
                });
            }
            else if (data.type === 'print') {
                let printReqData = {
                    'ProfitCenterID': localStorage.getItem('profitCenterId'),
                    'EmployeeID': localStorage.getItem('empId'),
                    'PrintInfo': item['PrintReceipt']
                };

                this.appBroadcastProvider.display(true);
                this.appServiceProvider.callPrintApi(printReqData).subscribe(printDataResponse => {
                    this.appBroadcastProvider.display(false);
                    this.errorHandlerProvider.handleError(this.selectedLanguageText.SUCCESSFULLY_PRINTED, 'success');
                },errResponse => {
                    this.appBroadcastProvider.display(false);
                    this.errorHandlerProvider.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
                });
            }
        });
        modalPage.present();
    }
}
