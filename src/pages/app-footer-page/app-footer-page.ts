// angular import section
import { Component, NgZone, OnInit } from '@angular/core';

// ionic import section
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { Subscription } from 'rxjs';


@IonicPage()
@Component({
    selector: 'app-footer-page',
    templateUrl: 'app-footer-page.html',
})
export class AppFooterPage implements OnInit {

    public footerText: any;
    public rightMenuSubscription: Subscription;

    constructor(public navCtrl: NavController, public navParams: NavParams, public appBroadcastProvider: AppBroadcastProvider, private zone:NgZone, ) {
    }

    ngOnInit() {
        this.rightMenuSubscription = this.appBroadcastProvider.subscribeScrollTextInfo().subscribe((message) => this.zone.run(() => {
            this.footerText = message ? message[0] : '';
        }));
    }
}

