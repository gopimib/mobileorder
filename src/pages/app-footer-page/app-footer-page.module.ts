import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppFooterPage } from './app-footer-page';

@NgModule({
    declarations: [
        AppFooterPage,
    ],
    imports: [
        IonicPageModule.forChild(AppFooterPage),
    ],
    exports: [
        AppFooterPage
    ]
})
export class AppFooterPageModule { }
