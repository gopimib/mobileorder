// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// 3rd party imports section
import { NgxQRCodeModule } from 'ngx-qrcode2';

// components imports section
import { TenderCheckPage } from './tender-check';
import { PinCheckPageModule } from '../pin-check/pin-check.module';

@NgModule({
    declarations: [
        TenderCheckPage,
    ],
    imports: [
        IonicPageModule.forChild(TenderCheckPage),
        NgxQRCodeModule,
        PinCheckPageModule
    ],
    exports: [
        TenderCheckPage
    ]
})
export class TenderCheckPageModule { }
