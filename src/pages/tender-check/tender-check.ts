// angular imports section
import { Component, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/interval';

// ionic imports section
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';


// 3rd party imports section
import $ from 'jquery';

import * as _ from 'lodash';

// providers imports section
import { AppServiceProvider } from '../../providers/app-service/app-service';
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { Dialogs } from 'ionic-native';

@IonicPage()
@Component({
    selector: 'page-tender-check',
    templateUrl: 'tender-check.html',
})

export class TenderCheckPage implements OnDestroy {

    public resultArray: any = {};
    public itemsandModifiersArray: Array<any> = [];
    public subTotal: number = 0;
    public total: any = 0;
    public tip: number = 0;
    public isShowPaidLabel: boolean = false;
    public disablePayOption: boolean = false;
    public isShowEmptyDiv: boolean = false;
    public createdCode: string = null;
    public defaultSettings: any;
    public sub: any;
    public pullApiSub: any;
    public tableNumber = this.navParams;
    public totalTax: number = 0;
    public selectedLanguageText: any;

    qrData = null;
    scannedCode = null;

    constructor(public navCtrl: NavController, public navParams: NavParams, private appServiceProvider: AppServiceProvider,
        private errorHandlerService: ErrorHandlerProvider, private appBroadcastProvider: AppBroadcastProvider, private modalCtrl: ModalController) {

        let data = { 'Table': this.appServiceProvider.getSelectedTableId(), 'ProfitCenterId': localStorage.getItem('profitCenterId') };
        this.defaultSettings = this.appServiceProvider.getDefaultSettings();
        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();

        this.resultArray.TaxAmount = 0;

        setTimeout(() => {
            this.appBroadcastProvider.display(true);
        }, 50);

        this.appServiceProvider.recallCheck(data).subscribe(data => {
            this.resultArray = data;
            let checkNumber = { 'CheckNumber': data['CheckNumber'] };

            if (this.resultArray.MenuItems && this.resultArray.MenuItems.length) {
                this.tip = 0;
                this.parseResponseData(this.resultArray);
                this.isShowEmptyDiv = false;
                
            }

            if ((this.resultArray && !this.resultArray.MenuItems.length && this.resultArray['CheckNumber'].toString() !== '0') || this.resultArray['CheckNumber'].toString() === '0') {
                this.isShowEmptyDiv = true;
            }

            if (data['CheckNumber'] !== '0') {
                this.appServiceProvider.getSplitCheck(checkNumber).subscribe(data => {
                    let count = 0;
                    if (data['splitcheckresponse'] && data['splitcheckresponse'].length) {
                        for (let i = 0; i < data['splitcheckresponse'].length; i++) {
                            if (data['splitcheckresponse'][i].Status === 'completed') {
                                count = 1;
                            }
                        }
                        if (count) {
                            this.errorHandlerService.handleError(this.selectedLanguageText.CHECK_SPLITTED_TENDER_OPTION, 'info');
                            this.disablePayOption = true;
                        }
                    }
                    this.appBroadcastProvider.display(false);
                }, err => {
                    this.appBroadcastProvider.display(false);
                });
                this.triggerDetailsDiv();
            } else {
                this.appBroadcastProvider.display(false);
                this.triggerDetailsDiv();
            }
        }, err => {
            this.appBroadcastProvider.display(false);
        });
    }

    /**
     * @method parseResponseData
     * @param data response data
     * @description used to parse response data
     */
    parseResponseData(data) {
        let menuDetails = this.appServiceProvider.getMenuDetails();
        let menuItem = menuDetails['ITEMLIST'].ITEM;
        let modifierDetails = this.appServiceProvider.getModifersData();
        let modifierData = modifierDetails['modifierlistresponse'];
        let selectedItems = data;
        let typeLang = localStorage.getItem('lang') === 'English' ? '_NAME' : '_ALT_NAME';
        let modifierLang = localStorage.getItem('lang') === 'English' ? 'ModifierName' : 'ModifierAltName';
        let addedModifier = selectedItems.MenuModifiers;

        for (let i = 0; i < selectedItems.MenuItems.length; i++) {
            for (let j = 0; j < menuItem.length; j++) {
                if (menuItem[j]['_ID'].toString() === selectedItems.MenuItems[i].Id.toString()) {
                    selectedItems.MenuItems[i].name = menuItem[j][typeLang];
                    selectedItems.MenuItems[i].type = 'Item';
                    selectedItems.MenuItems[i].amt = parseFloat(selectedItems.MenuItems[i].BillAmount).toFixed(2);
                    continue;
                }
            }
        }

        if (modifierData) {
            for (let i = 0; i < selectedItems.MenuModifiers.length; i++) {
                for (let j = 0; j < modifierData.length; j++) {
                    if (modifierData[j]['ModifierId'].toString() === selectedItems.MenuModifiers[i].ModifierId.toString()) {
                        selectedItems.MenuModifiers[i].BillAmount = modifierData[j]['ModifierPrice'];
                        selectedItems.MenuModifiers[i].Id = modifierData[j]['ModifierId'];
                        selectedItems.MenuModifiers[i].Quantity = '1';
                        selectedItems.MenuModifiers[i].type = 'Modifier';
                        selectedItems.MenuModifiers[i].name = modifierData[j][modifierLang] + '(' + this.defaultSettings['currencyType'] + parseFloat(selectedItems.MenuModifiers[i].BillAmount).toFixed(2) + ')';
                        continue;
                    }
                }
            }
        }

        for (let i = 0; i < selectedItems.MenuItems.length; i++) {
            this.itemsandModifiersArray.push(selectedItems.MenuItems[i]);
            for (let j = 0; j < selectedItems.MenuModifiers.length; j++) {

                let numberOfOccurenceInModifierArray = addedModifier.reduce(function (n, modifier) {
                    return n + (modifier.ModifierId === addedModifier[j].ModifierId);
                }, 0);

                let numberOfOccurenceInCombinedArray = this.itemsandModifiersArray.reduce(function (n, modifier) {
                    return n + (modifier.ModifierId === addedModifier[j].ModifierId);
                }, 0);

                let index = _.findIndex(this.itemsandModifiersArray, { ModifierId: selectedItems.MenuModifiers[j].ModifierId });
                let originalPrice = _.find(menuItem, { '_ID': parseInt(selectedItems.MenuItems[i].Id) || selectedItems.MenuItems[i].Id.toString() });

                if (selectedItems.MenuModifiers[j].AssociatedItemId === selectedItems.MenuItems[i].Id && (index === -1 || numberOfOccurenceInCombinedArray < numberOfOccurenceInModifierArray) &&
                    originalPrice._PRICE === selectedItems.MenuItems[i].BillAmount - selectedItems.MenuModifiers[j].BillAmount) {
                    this.itemsandModifiersArray.push(selectedItems.MenuModifiers[j]);
                    j = selectedItems.MenuModifiers.length;
                    continue;
                }
            }
        }

        for (let i = 0; i < selectedItems.MenuItems.length; i++) {
            this.subTotal = this.subTotal + parseFloat(selectedItems.MenuItems[i].BillAmount);
        }

        this.total = this.subTotal + this.resultArray.TaxAmount;
        this.subTotal = parseFloat(this.subTotal.toFixed(2));
        this.total = parseFloat(this.total.toFixed(2));
        $('.menu-div-right').removeClass('right-width-20');

        if ($('.menu-div-left').hasClass('left-width-20') || $('.menu-div-left').hasClass('width-10')) {
            this.appBroadcastProvider.showMenu(false, false);
        } else {
            this.appBroadcastProvider.showMenu(true, false);
        }

    }

    /**
     * @method payByCash
     * @description used to close check by cash tyoe
     */
    payByCash() {
        let reqData = {
            'Table': this.appServiceProvider.getSelectedTableId(),
            'CheckType': '1',
            'CheckNumber': this.resultArray.CheckNumber,
            'TenderId': '1',
            'TenderAmount': parseFloat(this.total).toFixed(2),
            'Tip': '0',
            'ProfitCenterId': localStorage.getItem('profitCenterId')
        };

        this.appBroadcastProvider.display(true);
        this.appServiceProvider.closeCheck(reqData).subscribe(closeCheckResponse => {
            this.appBroadcastProvider.display(false);
            if (closeCheckResponse['error']) {
                return true;
            }
            this.triggerDetailsDiv();
            this.isShowPaidLabel = true;
        //     let modalPage = this.modalCtrl.create('PinCheckPage', { 'option': 'print', 'printReceipt': closeCheckResponse['RECEIPT_TEXT'] });
        //     modalPage.onDidDismiss(data => {
        //         if (data.type === 'email') {

        //             const confirmPromise = Dialogs.prompt(this.selectedLanguageText.EMAIL_LABEL, '', [this.selectedLanguageText.SEND_BTN, this.selectedLanguageText.CANCEL_BTN], '');
        //             confirmPromise.then((result) => {
        //                 let emailId = result.input1;
        //                 let btnIndex = result.buttonIndex;

        //                 if (btnIndex === 1) {

        //                     if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailId)) {

        //                         let emailReqData = {
        //                             'ToAddress': emailId,
        //                             'CheckNumber': reqData['CheckNumber'],
        //                             'CheckAmount': this.defaultSettings['currencyType'] + parseFloat(reqData['TenderAmount']).toFixed(2),
        //                             'ReceiptText': closeCheckResponse['RECEIPT_TEXT']
        //                         }
        //                         this.appBroadcastProvider.display(true);
        //                         this.appServiceProvider.sendEmail(emailReqData).subscribe(emailDataResponse => {
        //                             this.appBroadcastProvider.display(false);
        //                             this.errorHandlerService.handleError(this.selectedLanguageText.MAIL_SENT_TEXT, 'success');
        //                         }, errResponse => {
        //                             this.appBroadcastProvider.display(false);
        //                             this.errorHandlerService.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
        //                         });
        //                     } else {
        //                         this.errorHandlerService.handleError(this.selectedLanguageText.INAVLID_MAIL, 'error');
        //                     }
        //                 }
        //             });
        //         }
        //         else if (data.type === 'print') {
        //             let printReqData = {
        //                 'ProfitCenterID': localStorage.getItem('profitCenterId'),
        //                 'EmployeeID': localStorage.getItem('empId'),
        //                 'PrintInfo': closeCheckResponse['RECEIPT_TEXT']
        //             };

        //             this.appBroadcastProvider.display(true);
        //             this.appServiceProvider.callPrintApi(printReqData).subscribe(printDataResponse => {
        //                 this.appBroadcastProvider.display(false);
        //                 this.errorHandlerService.handleError(this.selectedLanguageText.SUCCESSFULLY_PRINTED, 'success');
        //             }, errResponse => {
        //                 this.appBroadcastProvider.display(false);
        //                 this.errorHandlerService.handleError(this.selectedLanguageText.SOMTHING_UNEXPECTED_ERROR, 'error');
        //             });
        //         }
        //         this.appServiceProvider.setSelectedTableId('');
        //         this.disablePayOption = true;
        //     });
        //     modalPage.present();
        }, err => {
            this.appBroadcastProvider.display(false);
        });
    }

    replacedText(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    /**
     * @method generateAlipayQrCode
     * @description used to generate QR code for checks using ALIPAY api 
     */
    generateAlipayQrCode() {
        let reqData = {
            'Body': 'InfoGenesis',
            'RequestId': this.resultArray.CheckNumber.toString(),
            'Currency': 'USD',
            'Quantity': '1',
            'TotalFee': this.total,
            'PassbackParameters': 'Test Param',
            'SellerEmail': 'gopi.manickam@agilysis.com',
            'DeviceID': '12312321',
            'AppID': 'Com.Agilysys.EFMO'
        }
        this.appBroadcastProvider.display(true);

        this.appServiceProvider.doGenerateAlipayQrCode(reqData).subscribe(data => {
            this.createdCode = data['QRCode'];
            this.appBroadcastProvider.display(false);
            this.sub = Observable.interval(5000)
                .subscribe((val) => {
                    this.callPullApi(reqData.RequestId);
                });
        }, err => {
            this.appBroadcastProvider.display(false);
        });
    }

    /**
     * @method callPullApi
     * @param item check details
     * @description used to check alipay payment response
     */
    callPullApi(data) {

        if (this.createdCode) {
            if (this.pullApiSub) {
                this.pullApiSub.unsubscribe();
            }
            let reqData = {
                'RequestID': this.resultArray.CheckNumber.toString(),
            }
            this.pullApiSub = this.appServiceProvider.checkPullAPI(reqData).subscribe(data => {
                if (data['STATUS'] === 'SUCCESS') {
                    this.callCloseApi();
                } else if (data['STATUS'] === 'FAILED') {
                } else {
                }
            }, err => {
            });
        }
    }

    /**
     * @method callCloseApi
     * @description used to close the check
     */
    callCloseApi() {
        let reqData = {
            'table': this.appServiceProvider.getSelectedTableId(),
            'checktype': '1',
            'checknumber': this.resultArray.CheckNumber,
            'tenderid': '1',
            'tenderamt': this.total.toString(),
            'tip': this.tip.toString()
        };
        this.appServiceProvider.closeAlipayPaymentCheck(reqData).subscribe(data => {
            let modalPage = this.modalCtrl.create('PinCheckPage', { 'option': 'print' });

            modalPage.onDidDismiss(data => {
                this.createdCode = null;
                this.isShowPaidLabel = true;
                this.appServiceProvider.setSelectedTableId('');
                this.disablePayOption = true;
                this.appBroadcastProvider.callOpenOrdersApi();
            });
            modalPage.present();
        }, err => {
        });
    }

    /**
     * @method triggerDetailsDiv() event for ios issue fix
     */
    triggerDetailsDiv() {
        $('.details-div').on('click', function () {
        });
        $('.details-div').trigger('click');
    }

    /**
     * @method ngOnDestroy
     * @description used to do some actions while page destroy
     */
    ngOnDestroy() {
        this.appServiceProvider.setSelectedTableId('');
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }
}
