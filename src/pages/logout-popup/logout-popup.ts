// angular imports section
import { Component } from '@angular/core';

// ionic imports section
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

import { AppServiceProvider } from '../../providers/app-service/app-service';

@IonicPage()
@Component({
    selector: 'page-logout-popup',
    templateUrl: 'logout-popup.html',
})
export class LogoutPopupPage {

    public data = this.navParams;
    public messageText: string = '';
    public okBtnText: string = '';
    public selectedLanguageText: any;
    
    constructor(public viewCtrl: ViewController, public navParams: NavParams, public appServiceProvider: AppServiceProvider) {
        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
        this.messageText = this.data.data['option'] === 'logut' ? this.selectedLanguageText.LANGUAGE_CHANGE_TEXT : this.selectedLanguageText.REMOVE_ITEM_TEXT; 
        this.okBtnText = this.data.data['option'] === 'logut' ? this.selectedLanguageText.LOGOUT_BTN_TEXT : this.selectedLanguageText.OK_BTN_TEXT;
    }

    /**
     * @method closePopup
     * @param data 
     * @description used to close the popup
     */
    closePopup(data) {
        this.viewCtrl.dismiss(data);
    }

}
