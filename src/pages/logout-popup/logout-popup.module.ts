// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// component imports section
import { LogoutPopupPage } from './logout-popup';

@NgModule({
    declarations: [
        LogoutPopupPage,
    ],
    imports: [
        IonicPageModule.forChild(LogoutPopupPage),
    ],
    exports: [
        LogoutPopupPage
    ]
})
export class LogoutPopupPageModule { }
