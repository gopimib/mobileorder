// angular imports section
import { Component, AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/interval';


// ionic imports section
import { Device } from '@ionic-native/device';
import { NavController, LoadingController } from 'ionic-angular/index';
import { GESTURE_PRIORITY_TOGGLE } from 'ionic-angular/gestures/gesture-controller';

// 3rd party imports section
import $ from 'jquery';
import * as _ from 'lodash';

// config imports section
import { AppConfig } from '../../app/app.config';

// providers imports section
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';
import { AppServiceProvider } from '../../providers/app-service/app-service';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage implements OnInit, OnDestroy {

    public isShowAppSettingView: boolean = false;
    public isShowLandingPage: boolean = true;
    public isShowLeftMenu: boolean = false;
    public isShowLeftBurgerMenu: boolean = false;
    public isShowRightBurgerMenu: boolean = false;
    public isShowRightMenu: boolean = false;
    public isShowLoginPage: boolean = true;
    public showLoader: boolean = false;
    public langugeLoader: boolean = false;
    public menuItems: Array<Object> = [];
    public itemChoices: Array<string> = [];
    public showMenu: Array<string> = [];
    public selectedOpitonIdex: number = 0;
    public selectedMenuOptionIndex: number = -1;
    public selectedCheckOptionIndex: number = -1;
    public selectedMenuIndex: string = '0';
    public lang: string = 'en';
    public currentClass: string = 'currentLang';
    public orderStatusText: string = '';
    public checkMenuHeaderText: string = '';
    public logoutText: string = '';
    private openApiSubscription: Subscription;
    public menuSubscription: Subscription;
    public loginSubscription: Subscription;
    public rightMenuSubscription: Subscription;
    public translateDiv: any = '';
    public pullApi: any;
    public listItems: any = {};
    public resposeData: any = {};
    public resposeDataCopy: any = {};
    public selectedLanguageText: any = {};

    public isShowMenuIcon: boolean;
    public isShowItemIcon: boolean;
    private menuType: any;

    constructor(private appBroadcastProvider: AppBroadcastProvider, private loadingController: LoadingController, private appServiceProvider: AppServiceProvider,
        private device: Device) {

        this.appServiceProvider.setDeviceId(this.device.uuid);
        this.appServiceProvider.setFloorData('');
        this.appServiceProvider.setMenuDetails('');
        this.appServiceProvider.setModifersData('');

        let selectedLanguage = (localStorage.getItem('lang') === 'English' || localStorage.getItem('lang') === undefined || localStorage.getItem('lang') === '' || localStorage.getItem('lang') === null) ? 'english' : 'chinese';
        
        this.appServiceProvider.getLanguageBasedText(selectedLanguage).subscribe(response => {
            this.appServiceProvider.setSelectedLanguageTexts(response);
            let localLanguage = selectedLanguage === 'english' ? 'English' : 'Chinese';
            localStorage.setItem('lang', localLanguage);
            this.langugeLoader = true;
            this.setMenuItemData('fromConstructor');
            this.setSvgData();
            this.setLanguage();
        });

        this.loginSubscription = this.appBroadcastProvider.subscribeLogin().subscribe(message => {
            this.doLoginLogoutAction(message);
        });

        this.menuSubscription = this.appBroadcastProvider.isShowHideLeftOrRighttMenu().subscribe(message => {
            this.doLeftMenuRightMenuAction(message);
        });

        this.appBroadcastProvider.loader.subscribe((val: boolean) => {
            this.showLoader = val;
        });

        this.rightMenuSubscription = this.appBroadcastProvider.subscribeMenuOptionEvent().subscribe(message => {
            if (message.index === -1) {
                this.selectedMenuOptionIndex = 0;
            }
        });
    }

    ngOnInit() {
    }

    /**
     * @method toggleItemHeader
     * @description used to do change the item images into button and vice versa
     */
    toggleItemHeader() {
        this.appBroadcastProvider.toggleMenuItemView(this.isShowItemIcon);
    }

    /**
     * @method toggleMenuHeader
     * @description used to do change the header text into icon and vice versa 
     */
    toggleMenuHeader() {
    }

    /**
     * @method doLeftMenuRightMenuAction
     * @param message  subscribe object value
     * @description used to do changes while clicking left or right burger menu
     */
    doLeftMenuRightMenuAction(message) {

        this.isShowRightBurgerMenu = false;
        this.isShowLeftBurgerMenu = false;

        if (!message.showLeft && !message.showRight) {
            this.isShowRightBurgerMenu = false;
            this.isShowLeftBurgerMenu = false;
        } else if (message.showLeft && !message.showRight) {

            this.isShowLeftBurgerMenu = true;
            this.isShowRightMenu = false;
            this.isShowLeftMenu = true;

            if (this.selectedCheckOptionIndex === -1) {
                this.openMenuItem('Open Check', this.selectedCheckOptionIndex, 'click');
            }

        } else if (!message.showLeft && message.showRight) {

            this.isShowRightBurgerMenu = true;
            this.isShowRightMenu = true;
            this.isShowLeftMenu = false;

            let typeLang = localStorage.getItem('lang') === 'English' ? '_TYPE' : '_TYPE_ALT_NAME';
            let data = this.appServiceProvider.getMenuDetails();

            if (data && data['ITEMLIST'].ITEM && data['ITEMLIST'].ITEM.length) {
                this.resposeData = data['ITEMLIST'].ITEM;
                this.resposeDataCopy = _.cloneDeep(data['ITEMLIST'].ITEM);
                this.itemChoices = this.unique(this.resposeData, '_TYPE_ID');
                this.showMenu = this.unique(this.resposeData, typeLang);
                if (this.selectedMenuOptionIndex === -1) {
                    this.openMenuChoices(this.showMenu[0], this.selectedMenuOptionIndex, 'click');
                    this.selectedMenuOptionIndex = 0;
                }
            } else {
                this.setMenuItemData('fromSubscribe');
            }
        }
        if (this.isShowLeftBurgerMenu) {
            $('.right-menu-div').css({ 'z-index': '9999' });
        } else {
            $('.right-menu-div').css({ 'z-index': '0' });
        }
        if (this.isShowRightBurgerMenu) {
            $('.menu-div').css({ 'z-index': '9999' });
        } else {
            $('.menu-div').css({ 'z-index': '0' });
        }

        this.setContentViewWidthHeight();
    }

    /**
     * @method doLoginLogoutAction
     * @param message  subscribe object value
     * @description used to do changes in login and logout actions
     */
    doLoginLogoutAction(message) {

        this.setFalseValue();
        this.isShowLoginPage = !message.isDoLogin;

        if (!this.isShowLoginPage) {
            setTimeout(() => {
                this.isShowAppSettingView = true;
                this.showOrhideLandingPage();
                this.isShowMenuIcon = this.appServiceProvider.getDefaultSettings().isIconEnabled === 'YES' ? true : false;
                this.isShowItemIcon = this.appServiceProvider.getDefaultSettings().isImagesEnabled === 'YES' ? true : false;
                this.openMenuItem('Open Check', this.selectedCheckOptionIndex, 'click');
                this.setLanguage();
                this.openApiSubscription = Observable.interval(60000)
                    .subscribe((val) => { this.callOpenTablesApi(); });
            }, 0);
        } else {
            setTimeout(() => {
                //this.clearLoginDom();
            }, 100);
        }
    }

    /**
     * @method setMenuItemData
     * @description used to set menu item data locally
     */
    setMenuItemData(val) {

        let menuItems = this.appServiceProvider.getMenuDetails();
        let typeLang = localStorage.getItem('lang') === 'English' ? '_TYPE' : '_TYPE_ALT_NAME';

        if (menuItems && menuItems['ITEMLIST'] && menuItems['ITEMLIST'].ITEM && menuItems['ITEMLIST'].ITEM.length) {
            this.appServiceProvider.setMenuDetails(menuItems);
        } else {
            if (localStorage.getItem('profitCenterId')) {
                let data = { 'profitcenterid': localStorage.getItem('profitCenterId'), 'terminaltype': '1' };
                this.appServiceProvider.getDataAndChoice(data).subscribe(localData => {

                    if (localData && localData['ITEMLIST'] && localData['ITEMLIST'].ITEM && localData['ITEMLIST'].ITEM.length) {

                        this.resposeData = localData['ITEMLIST'].ITEM;
                        this.resposeDataCopy = _.cloneDeep(localData['ITEMLIST'].ITEM);
                        this.itemChoices = this.unique(this.resposeData, '_TYPE_ID');
                        this.showMenu = this.unique(this.resposeData, typeLang);
                        this.listItems = _.groupBy(this.resposeData, '_TYPE_ID');
                        this.appServiceProvider.setMenuDetails(localData);

                        if (val === 'fromSubscribe') {
                            if (this.selectedMenuOptionIndex === -1) {
                                this.openMenuChoices(this.itemChoices[0], this.selectedMenuOptionIndex, 'click');
                                this.selectedMenuOptionIndex = 0;
                            }
                        }
                    }
                }, err => {
                });
            }
        }
    }

    /**
     * @method setSvgData
     * @description used to set svg data locally
     */
    setSvgData() {

        let svgData = this.appServiceProvider.getFloorData();

        if (svgData && svgData['svg'] && svgData['Height'] && svgData['Width']) {
            this.appServiceProvider.setFloorData(svgData);
        } else {
            if (localStorage.getItem('profitCenterId')) {
                let screenWidth = $('.content-view').width();
                let data = { 'profitcenterid': localStorage.getItem('profitCenterId'), 'screenWidth': screenWidth };
                this.appServiceProvider.getFloorStructure(data).subscribe(svgData => {
                    this.appServiceProvider.setFloorData(svgData);
                });
            }
        }
    }

    /**
     * @method setLanguage
     * @description used to load appropriate text based on language type
     */
    setLanguage() {
        this.selectedLanguageText = this.appServiceProvider.getSelectedLanguageTexts();
        this.menuType = this.selectedLanguageText.CHECK_MENU_TITLES;
        this.setCheckMenuItems();
    }

    /**
     * @method setCheckMenuItems
     * @description used to set static menu items
     */
    setCheckMenuItems() {
        this.menuItems = [
            { 'category': this.menuType[0], 'isSelected': false, 'imageUrl': './assets/imgs/OpenCheckTrans.png' },
            { 'category': this.menuType[1], 'isSelected': false, 'imageUrl': './assets/imgs/RecallTrans.png' },
            { 'category': this.menuType[2], 'isSelected': false, 'imageUrl': './assets/imgs/SplitTrans.png' },
            { 'category': this.menuType[3], 'isSelected': false, 'imageUrl': './assets/imgs/TenderTrans.png' },
            { 'category': this.menuType[4], 'isSelected': false, 'imageUrl': './assets/imgs/PrintTrans.png' },
            { 'category': this.menuType[5], 'isSelected': false, 'imageUrl': './assets/imgs/OrderTrans.png' }
        ];
    }

    /**
     * @method unique
     * @param arr array of objects
     * @param prop key value of the object
     * @description used to group the array of objects bease on one key value pair
     */
    unique(arr, prop) {
        return arr.map(function (e) { return e[prop]; }).filter(function (e, i, a) {
            return i === a.indexOf(e);
        });
    }

    /**
     * @method setContentViewWidthHeight
     * @description used to set height and width for test div and modify width and height for left and right menu
     */
    setContentViewWidthHeight() {

        setTimeout(() => {
            let height = $('.right-menu-style .menu-content-div').height();
            let width = $('.right-menu-style .menu-content-div').width();
            $('.test').css('height', '100%');
            $('.test').css('width', '100%');
            if (!this.isShowRightBurgerMenu) {
                $('.left-menu-style img').width('100%');
                $('.left-menu-style .menu-img-div').css({ 'width': '25%' });
                $('.left-menu-style .image-div').css({ 'height': '140px !important' });
            } else {
                $('.left-menu-style img').width('100%');
                $('.left-menu-style .menu-img-div').css({ 'width': '25%' });
                $('.left-menu-style .image-div').css({ 'height': '140px !important' });
            }
        }, 300);
    }

    /**
     * @method logout
     * @description used to logout
     */
    logout() {
        localStorage.setItem('profitCenterId', '');
        localStorage.setItem('empId', '');
        this.appServiceProvider.setAddMoreItemsValues('', '');
        this.appServiceProvider.setCheckOptionDetail('');
        this.appServiceProvider.setMenuDetails('');
        this.appServiceProvider.setModifersData('');
        this.appBroadcastProvider.showMenu(false, false);
        this.appBroadcastProvider.publishDoLogin(false);
        this.appBroadcastProvider.display(false);
    }

    /**
     * @method clearLoginDom
     * @description used to destroy all DOM elements
     */
    clearLoginDom() {
        this.isShowLoginPage = true;
        this.isShowLeftBurgerMenu = false;
        this.isShowLandingPage = false;
        this.isShowLeftMenu = false;
        this.isShowRightMenu = false;
        this.openApiSubscription.unsubscribe();
    }

    /**
     * @method setFalseValue
     * @description used to set deafult values for some boolean variable
     */
    setFalseValue() {

        this.isShowAppSettingView = false;
        this.isShowLandingPage = true;
        this.isShowLeftMenu = false;
        this.isShowRightMenu = false;
        this.isShowLoginPage = false;
        this.selectedCheckOptionIndex = -1;
        this.selectedMenuOptionIndex = -1;
    }

    /**
     * @method showOrhideLandingPage
     * @description used to show or hide landingpage view
     */
    showOrhideLandingPage() {

        this.isShowLandingPage = false;
        if ($('.menu-content-div')) {
            this.appBroadcastProvider.setMenuContentHeightInfo();
        }
    }

    /**
     * @method resetView
     * @description used to reset the view
     */
    resetView() {
        this.isShowLeftMenu = false;
        this.isShowRightMenu = false;
        this.isShowLandingPage = false;
    }

    /**
     * @method openMenuItem
     * @param category selected check type name
     * @param index selected check type index
     * @param fromMethod type of event (click or method)
     * @description used to change the svg image actions based on the selected check type
     */
    openMenuItem(category, index, fromMethod) {

        this.selectedCheckOptionIndex = index;
        this.showOrhideLandingPage();
        if (index === 5) {
            this.appBroadcastProvider.publishCheckOptionEvent('order', this.menuItems.length - 1, 'method');
            this.appBroadcastProvider.display(true);
        } else if (index === 4) {
            this.appBroadcastProvider.publishCheckOptionEvent('print', this.menuItems.length - 1, 'method');
            this.appBroadcastProvider.display(true);
        } else {
            if (index === -1) {
                index = 0;
            }
            this.appBroadcastProvider.publishCheckOptionEvent(category, index, fromMethod);
        }
        if (this.selectedCheckOptionIndex !== -1) {
            //this.closeMenu('check');
        } else {
            this.selectedCheckOptionIndex = 0;
        }
        if (index === 0) {
            category = localStorage.getItem('lang') === 'Chinese' ? AppConfig.CHINESE_TEXT.CHECK_MENU_TITLES[0] : AppConfig.ENGLISH_TEXT.CHECK_MENU_TITLES[0];
        }
        this.appBroadcastProvider.publishSettingsHeader(category);
    }

    /**
     * @method closeMenu
     * @description used to close the both left and right menu
     */
    closeMenu(val) {
        if (val === 'check') {
            this.appBroadcastProvider.showMenu(false, false);
        }
    }

    /**
     * @method openMenuChoices
     * @param category selected check type name
     * @param index selected check type index
     * @param fromMethod type of event (click or method)
     * @description used to call menuItemClick in left-menu.ts file
     */
    openMenuChoices(category, index, fromMethod) {

        this.selectedMenuOptionIndex = index;
        this.showOrhideLandingPage();
        if (this.selectedMenuOptionIndex === -1 || this.selectedMenuOptionIndex === undefined) {
            this.selectedMenuOptionIndex = 0;
        }
        this.appBroadcastProvider.publishMenuOptionEvent(category, this.selectedMenuOptionIndex, fromMethod);
    }

    /**
     * @method onAddToCheck
     * @description used to call add to check mehtod in left-menu.ts 
     */
    onAddToCheck() {
        this.appBroadcastProvider.publishAddToCheckMethod();
    }

    /**
     * @method callOpenTablesApi
     * @description used to call open order table API
     */
    callOpenTablesApi() {

        if (this.pullApi) {
            this.pullApi.unsubscribe();
        }

        this.pullApi = this.appServiceProvider.getOpenOrdersDetails().subscribe(openTableData => {

            if (!openTableData['error']) {
                let result = _.map(openTableData['openorderstable'], 'Table');
                this.appServiceProvider.setOpenCheckDetails(result);
            }
            this.appBroadcastProvider.display(false);
        });
    }

    /**
     * @method ngOnDestroy
     * @description used to do some actions while page destroy
     */
    ngOnDestroy() {
        this.openApiSubscription.unsubscribe();
        this.loginSubscription.unsubscribe();
        this.menuSubscription.unsubscribe();
    }
}

