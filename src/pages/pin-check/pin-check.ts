
// angular imports section
import { Component, AfterViewInit } from '@angular/core';

// ionic imports section
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

// services imports section
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';
import { AppServiceProvider } from '../../providers/app-service/app-service';

@IonicPage()
@Component({
    selector: 'page-pin-check',
    templateUrl: 'pin-check.html',
})
export class PinCheckPage implements AfterViewInit {

    public pin: string = '';
    public isRotateMainContent: boolean = false;
    public data = this.navParams;
    public popupTexts = {};
    public isShowPinText: boolean = true;
    public receiptDeatils: Array<any> = [];
    public selectedLaguageText: any;

    constructor(public viewController: ViewController, public navParams: NavParams, public errorHandlerProvider: ErrorHandlerProvider, public appServiceProvider: AppServiceProvider) {
        
        let data = this.data.data['printReceipt'] !== null ? this.replacedText(this.data.data['printReceipt'], ' ', '&nbsp') : '';
        this.receiptDeatils = data.split('\r\n');
        this.selectedLaguageText = this.appServiceProvider.getSelectedLanguageTexts();

    }

    replacedText(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    ngAfterViewInit() {
    }

    /**
     * @method closePopup
     * @param unLock 
     * @description used to close the popup
     */
    closePopup(value, type) {

        let data = { 'type': type, 'value': value };
        this.viewController.dismiss(data);
    }

    /**
     * @method keyValidation
     * @description used to validate the confirmation key
     */
    keyValidation() {

        let data = '1234';
        let unLock: boolean = false;
        if (this.pin && (this.pin.toString() === data)) {
            unLock = true;
            event.preventDefault();
        } else {
            unLock = false;
        }
    }

    /**
     * @method onFocus
     * @description used to add the rotate css
     */
    onFocus() {
        this.isRotateMainContent = true;
    }

    /**
     * @method onBlur
     * @description used to remove the rotate css
     */
    onBlur() {
        this.isRotateMainContent = false;
    }
}
