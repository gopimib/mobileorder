// angular imports section
import { NgModule } from '@angular/core';

// ionic imports section
import { IonicPageModule } from 'ionic-angular';

// component imports section
import { PinCheckPage } from './pin-check';

@NgModule({
    declarations: [
        PinCheckPage,
    ],
    imports: [
        IonicPageModule.forChild(PinCheckPage),
    ],
    exports: [
        PinCheckPage
    ]
})
export class PinCheckPageModule { }
