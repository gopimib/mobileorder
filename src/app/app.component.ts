import { Component, ViewChild, Pipe, PipeTransform, EventEmitter } from '@angular/core';

// ionic imports section
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AppConfig } from './app.config';

// components imports section
import { HomePage } from '../pages/home/home';
import { ErrorHandlerProvider } from '../providers/error-handler/error-handler';
import { AppBroadcastProvider } from '../providers/app-broadcast/app-broadcast';
import { AppServiceProvider } from '../providers/app-service/app-service';

declare var $: any;

@Component({
    templateUrl: 'app.html'
})

export class MyApp {
    rootPage: any = HomePage;
    @ViewChild(Nav) navChild: Nav;

    private connection: any;
    private proxy: any;
    private ulr: any;

    constructor(private platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private errorHandlerProvider: ErrorHandlerProvider,
        private appBroadcastProvider: AppBroadcastProvider, private appServiceProvider: AppServiceProvider) {
        this.platform.ready().then(() => {
            statusBar.styleDefault();
            splashScreen.hide();
        });

        localStorage.setItem('profitCenterId', '');
        localStorage.setItem('empId', '');

        this.connection = $.hubConnection(AppConfig.baseUrl + 'Signalr');
        this.proxy = this.connection.createHubProxy('ChatHub1');
        let currentObj = this;
        this.proxy.on('updateData', function (message) {
            currentObj.appBroadcastProvider.callScrollTextInfo(message);
        });

        this.connection.start({ withCredentials: false }).done((data: any) => {
            this.sendMessage();
        }).catch((error: any) => {
        });
    }

    public sendMessage(): void {
        this.proxy.invoke('RegisterConId', 'efmoFooterScrollText')
            .catch((error: any) => {
            });
    }
}

