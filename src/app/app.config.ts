export class AppConfig {
    public static baseUrl = 'http://10.17.38.45/';
    public static OK_BUTTON = 'OK';
    public static CLOSE_BUTTON = 'CLOSE';
    public static DEFAULT_TIMEOUT_SECONDS = 180000;
    public static COVER_COUNT_POPUP = {
        'COVER_COUNT_LABEL': 'Cover Count',
        'COVER_COUNT_POPUP_OK': 'Menu'
    };
    public static MODIFIERS_POPUP = {
        'MODIFIERS_POPUP_HEADER': 'Modifiers',
        'MODIFIERS_POPUP_OK': 'OK',
        'MODIFIERS_POPUP_CANCEL': 'CLOSE'
    };
    public static RECALL_POPUP = {
        'RECALL_POPUP_HEADER': 'Recall Check',
        'RECALL_POPUP_OK': 'ADD MORE',
        'RECALL_POPUP_CANCEL': 'CLOSE'
    };
    public static CONFIRMATION_POPUP = {
        'CONFIRMATION_POPUP_HEADER': 'Confirmation Popup',
        'CONFIRMATION_POPUP_OK': 'CONFIRM',
        'CONFIRMATION_POPUP_CANCEL': 'CLOSE',
        'EMPTY_MODIFIER_TEXT': 'Add Modifer'
    };
    //english translation
    public static ENGLISH_TEXT = {
        'CHECK_MENU_TITLES': ['Open Check', 'Recall Check', 'Split Check', 'Tender Check', 'Print Receipt', 'Order Status'],
        'ORDER_STATUS_TEXT': 'Order Status',
        'CHECK_MENU_HEADER': 'Checks',
        'EMPLOYEE_NUMBER_PLACEHOLDER': 'Employee Number',
        'PIN_NUMBER_PLACEHOLDER': 'Pin Number',
        'PROFIT_CENTER_PLACEHOLDER': 'Profit Center Id',
        'LOGIN_BTN_TEXT': 'Login',
        'LOGIN_VALIDATION_ERROR_TEXT': 'Please enter valid credentials',
        'LOGOUT': 'Logout',
        'LOGIN_FORM_FIELDS_MANDATORY_ERROR': 'Please enter all the details'
    }
    //chinese translation
    public static CHINESE_TEXT = {
        'CHECK_MENU_TITLES': ['打开检查', '召回检查', '拆分检查', '投标检查', '打印收据', '订单状态'],
        'ORDER_STATUS_TEXT': '訂單狀態',
        'CHECK_MENU_HEADER': '檢查',
        'EMPLOYEE_NUMBER_PLACEHOLDER': '員工編號',
        'PIN_NUMBER_PLACEHOLDER': '針號',
        'PROFIT_CENTER_PLACEHOLDER': '利润中心ID',
        'LOGIN_BTN_TEXT': '登錄',
        'LOGIN_VALIDATION_ERROR_TEXT': '请输入有效的凭证',
        'LOGOUT': '登出',
        'LOGIN_FORM_FIELDS_MANDATORY_ERROR': '请输入所有细节'
    }
}
