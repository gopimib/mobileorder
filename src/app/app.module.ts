// angular imports section
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpHeaders, HttpClient, HTTP_INTERCEPTORS, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ErrorHandler, NgModule, InjectionToken, Injectable, Inject } from '@angular/core';
import { Component, ViewChild, Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser'
import { TimeoutError } from 'rxjs/util/TimeoutError';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/timeout';

// ionic imports section
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Device } from '@ionic-native/device';
import { Dialogs } from 'ionic-native';

// 3rd party imports section
import { NgxQRCodeModule } from 'ngx-qrcode2';

// components imports section
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

// modules imports section
import { AppLandingPageModule } from '../pages/app-landing-page/app-landing-page.module';
import { AppSettingPageModule } from '../pages/app-setting-page/app-setting-page.module';
import { AppFooterPageModule } from '../pages/app-footer-page/app-footer-page.module';
import { AppLeftMenuPageModule } from '../pages/app-left-menu/app-left-menu.module';
import { AppRightMenuPageModule } from '../pages/app-right-menu/app-right-menu.module';
import { DirectivesModule } from '../directives/directives.module';
import { OpenCheckPageModule } from '../pages/open-check/open-check.module';
import { TenderCheckPageModule } from '../pages/tender-check/tender-check.module';
import { LoginPageModule } from '../pages/login/login.module';
import { PinCheckPageModule } from '../pages/pin-check/pin-check.module';

//config imports section
import { AppConfig } from './app.config';

// provider imports section
import { AppServiceProvider } from '../providers/app-service/app-service';
import { AppBroadcastProvider } from '../providers/app-broadcast/app-broadcast';
import { ErrorHandlerProvider } from '../providers/error-handler/error-handler';
import { HttpInterceptorProvider } from '../providers/http-interceptor/http-interceptor';

const DEFAULT_TIMEOUT = new InjectionToken<number>('defaultTimeout');
const defaultTimeout = AppConfig.DEFAULT_TIMEOUT_SECONDS;

@NgModule({
    declarations: [
        MyApp,
        HomePage,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppLandingPageModule,
        AppSettingPageModule,
        AppFooterPageModule,
        AppLeftMenuPageModule,
        AppRightMenuPageModule,
        DirectivesModule,
        OpenCheckPageModule,
        TenderCheckPageModule,
        LoginPageModule,
        IonicModule.forRoot(MyApp),
        NgxQRCodeModule,
        PinCheckPageModule
    ],
    exports: [
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {
            provide: ErrorHandler, useClass: IonicErrorHandler,
        },
        ScreenOrientation,
        AppBroadcastProvider,
        AppServiceProvider,
        Device,
        Dialogs,
        [{ provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorProvider, multi: true }],
        [{ provide: DEFAULT_TIMEOUT, useValue: defaultTimeout }],
        HttpInterceptorProvider,
        ErrorHandlerProvider
    ]
})
export class AppModule { }
