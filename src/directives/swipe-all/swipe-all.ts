// angular imports section
import { Directive, ElementRef, Input, OnInit, OnDestroy } from '@angular/core';

// ionic imports section
import {Gesture} from 'ionic-angular/gestures/gesture'

// providers imports section
import { AppBroadcastProvider } from '../../providers/app-broadcast/app-broadcast';

declare var Hammer: any

@Directive({
  selector: '[swipe-all]'
})

export class SwipeAllDirective  implements OnInit, OnDestroy {

    @Input('swipeUp') actionUp: any;
    @Input('swipeDown') actionDown: any;
    @Input('swipeLeft') actionLeft: any;
    @Input('swipeRight') actionRight: any;

    private el: HTMLElement
    private swipeGesture: Gesture
    private swipeDownGesture: Gesture

    constructor(el: ElementRef, private appBroadcastProvider: AppBroadcastProvider) {
        this.el = el.nativeElement
    }

    /**
     * @method ngOnInit
     * @description Used to initialize swipe events
     */
    ngOnInit() {
        
        this.swipeGesture = new Gesture(this.el, {
            recognizers: [
                [Hammer.Swipe]
            ]
        });

        this.swipeGesture.listen();

        this.swipeGesture.on('swipeup', e => {
        });

        this.swipeGesture.on('swipedown', e => {
        });

        this.swipeGesture.on('swipeleft', e => {
        });

        this.swipeGesture.on('swiperight', e => {
        });
    }

    /**
     * @method ngOnDestroy
     * @description Used to destroy swipe events
     */
    ngOnDestroy() {
        this.swipeGesture.destroy()
    }
}
