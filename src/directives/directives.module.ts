// angular imports section
import { NgModule } from '@angular/core';

// directives imports section
import { SwipeAllDirective } from './swipe-all/swipe-all';
import { LongPressDirective } from './long-press/long-press';

@NgModule({
    declarations: [
        SwipeAllDirective,
        LongPressDirective
    ],
    imports: [],
    exports: [
        SwipeAllDirective,
        LongPressDirective
    ]
})
export class DirectivesModule { }
