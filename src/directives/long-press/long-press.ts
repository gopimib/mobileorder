// angular imports section
import { Directive, ElementRef } from '@angular/core';

// ionic imports section
import { Gesture } from 'ionic-angular';

declare var Hammer: any

@Directive({
    selector: '[long-press]' // Attribute selector
})

export class LongPressDirective {

    el: HTMLElement;
    pressGesture: Gesture;

    constructor(el: ElementRef) {
        this.el = el.nativeElement;
    }

    /**
     * @method ngOnInit
     * @description Used to initialize pressGesture events
     */
    ngOnInit() {
        this.pressGesture = new Gesture(this.el, {
            recognizers: [
                [Hammer.Press, { time: 3000 }]
            ]
        });
        this.pressGesture.listen();
        this.pressGesture.on('press', e => {
        });
    }

    /**
     * @method ngOnDestroy
     * @description Used to destroy pressGesture events
     */
    ngOnDestroy() {
        this.pressGesture.destroy();
    }

}
