// angular import section
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AppBroadcastProvider {

    private subjectSwipe = new Subject<any>();
    private isShowTenderSubject = new Subject<any>();
    private isDoLogin = new Subject<any>();
    private setHeight = new Subject<any>();
    private loderInfo = new Subject<any>();
    private checkOptions = new Subject<any>();
    private menuOptions = new Subject<any>();
    private showHideLeftOrRighttMenu = new Subject<any>();
    private addToCheck = new Subject<any>();
    private settingsHeader = new Subject<any>();
    private tableNumber = new Subject<any>();
    private openOrder = new Subject<any>();
    private showScrollText = new Subject<any>();
    private showImages = new Subject<any>();
    private removeItem = new Subject<any>();
    
    public loader: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public updateItemCount: BehaviorSubject<any> = new BehaviorSubject<any>({});
    public setSelectedLanguage: BehaviorSubject<any> = new BehaviorSubject<any>({});

    constructor() { }

    /**
     * @name publishSwipeEvent(swipeEvent)
     * @param swipe event object
     * @description used to call subscribeSwipeEvent and tell the swipe postion and swipe method type
     */
    publishSwipeEvent(swipeEvent: any, from: any) {
        this.subjectSwipe.next({ text: swipeEvent, fromMethod: from });
    }

    /**
     * @name subscribeSwipeEvent()
     * @description used to tell the swipe postion and swipe method type to components which subscribe this event
     */
    subscribeSwipeEvent(): Observable<any> {
        return this.subjectSwipe.asObservable();
    }

    /**
     * @name publishTenderCheckPage(isShow)
     * @param isShow boolean variable
     * @description used to call subscribeTenderCheckPage and tell the isShow variable value
     */
    publishTenderCheckPage(isShow: boolean) {
        this.isShowTenderSubject.next({ isShowTenderCheckPage: isShow });
    }

    /**
     * @name subscribeTenderCheckPage()
     * @description triggered after publishTenderCheckPage method called
     */
    subscribeTenderCheckPage(): Observable<any> {
        return this.isShowTenderSubject.asObservable();
    }

    /**
     * @name publishDoLogin(isDoLogin)
     * @param isDoLogin boolean variable
     * @description used to call subscribeLogin and tell the isDoLogin variable value
     */
    publishDoLogin(isDoLogin: boolean, ) {
        this.isDoLogin.next({ isDoLogin: isDoLogin });
    }

    /**
     * @name subscribeLogin()
     * @description triggered after publishDoLogin method called
     */
    subscribeLogin(): Observable<any> {
        return this.isDoLogin.asObservable();
    }

    /**
     * @name setMenuContentHeightInfo()
     * @param isDoLogin boolean variable
     * @description used to set the height and width for the content area
     */
    setMenuContentHeightInfo() {
        this.setHeight.next();
    }

    /**
     * @name subscribeMenuContentHeightInfo()
     * @description triggered after setMenuContentHeightInfo method called
     */
    subscribeMenuContentHeightInfo(): Observable<any> {
        return this.setHeight.asObservable();
    }

    /**
     * @name setLoder(isShow)
     * @param isShow boolean variable
     * @description used to show or hide loader based on isShow variable
     */
    setLoder(isShow: boolean) {
        this.loderInfo.next({ isShow: isShow });
    }

    /**
     * @name subscribeLoderInfo()
     * @description triggered after setLoder method called
     */
    subscribeLoderInfo(): Observable<any> {
        return this.loderInfo.asObservable();
    }

    /**
     * @name showMenu()
     * @param showLeft boolean variable
     * @param showRight boolean variable
     * @description used to show or hide left and right menu
     */
    showMenu(showLeft: boolean, showRight: boolean) {
        this.showHideLeftOrRighttMenu.next({ showLeft: showLeft, showRight: showRight });
    }

    /**
     * @name isShowHideLeftOrRighttMenu()
     * @description triggered after showMenu method called
     */
    isShowHideLeftOrRighttMenu(): Observable<any> {
        return this.showHideLeftOrRighttMenu.asObservable();
    }

    /**
     * @name publishCheckOptionEvent()
     * @param category name of the selectd check type
     * @param index index position of the selectd check type
     * @param fromMethod from method type
     * @description used to publish selected check type option details
     */
    publishCheckOptionEvent(category: any, index: any, fromMethod: any) {
        this.checkOptions.next({ category: category, index: index, fromMethod: fromMethod });
    }

    /**
     * @name subscribeCheckOptionEvent()
     * @description triggered after publishCheckOptionEvent method called
     */
    subscribeCheckOptionEvent(): Observable<any> {
        return this.checkOptions.asObservable();
    }

    /**
     * @name publishMenuOptionEvent()
     * @param category name of the selectd menu type
     * @param index index position of the selected menu type
     * @param fromMethod from method type
     * @description used to publish selected menu type option details
     */
    publishMenuOptionEvent(category: any, index: any, fromMethod: any) {
        this.menuOptions.next({ category: category, index: index, fromMethod: fromMethod });
    }

    /**
     * @name subscribeMenuOptionEvent()
     * @description triggered after publishMenuOptionEvent method called
     */
    subscribeMenuOptionEvent(): Observable<any> {
        return this.menuOptions.asObservable();
    }

    /**
     * @name publishAddToCheckMethod()
     * @description used to trigger add to check method in left-menu.ts file
     */
    publishAddToCheckMethod() {
        this.addToCheck.next();
    }

    /**
     * @name subscribeAddToCheckMethod()
     * @description triggered after publishAddToCheckMethod method called
     */
    subscribeAddToCheckMethod(): Observable<any> {
        return this.addToCheck.asObservable();
    }

    /**
     * @name publishAddToCheckMethod()
     * @param checkType selected check type
     * @description used to set the select check type in app-setting.html
     */
    publishSettingsHeader(checkType: string) {
        this.settingsHeader.next({ header: checkType });
    }

    /**
     * @name subscribeAddToCheckMethod()
     * @description triggered after publishSettingsHeader method called
     */
    subscribeSettingsHeader(): Observable<any> {
        return this.settingsHeader.asObservable();
    }

    /**
     * @name updateItemCountValue()
     * @param details selected item details
     * @description used to update count of the item in left-menu.ts file
     */
    updateItemCountValue(details: any) {
        let data = { 'details': details };
        this.updateItemCount.next(data);
    }

    /**
     * @name publishTableNumber()
     * @param tableNumber selected table number
     * @description used to set the select table number
     */
    publishTableNumber(tableNumber: string) {
        this.tableNumber.next({ tableNumber: tableNumber });
    }

    /**
     * @name subscribeTableNumber()
     * @description triggered after publishTableNumber method called
     */
    subscribeTableNumber(): Observable<any> {
        return this.tableNumber.asObservable();
    }

    /**
     * @name display()
     * @param isShow boolean value
     * @description used to show or hide loader
     */
    display(isShow: boolean) {
        this.loader.next(isShow);
    }

    /**
     * @name callOpenOrdersApi()
     * @description used to call open order api
     */
    callOpenOrdersApi() {
        this.openOrder.next();
    }

    /**
     * @name callOpenOrdersApi()
     * @descriptiontriggered after callOpenOrdersApi method called
     */
    subscribeOpenOrdersApi() {
        return this.openOrder;
    }

    /**
     * @name callOpenOrdersApi()
     * @description used to call open order api
     */
    callScrollTextInfo(message) {
        this.showScrollText.next(message);
    }

    /**
     * @name callOpenOrdersApi()
     * @descriptiontriggered after callOpenOrdersApi method called
     */
    subscribeScrollTextInfo(): Observable<any> {
        return this.showScrollText.asObservable();
    }

    /**
     * @name toggleMenuItemView()
     * @description used to change view of the item list page
     */
    toggleMenuItemView(message) {
        this.showImages.next(message);
    }

    /**
     * @name subscribeMenuItemView()
     * @descriptiontriggered after toggleMenuItemView method called
     */
    subscribeMenuItemView(): Observable<any> {
        return this.showImages.asObservable();
    }


    /**
     * @name publishRemoveOption()
     * @description used to change view of the item list page
     */
    publishRemoveOption(message) {
        this.removeItem.next(message);
    }

    /**
     * @name subscribeRemoveOption()
     * @descriptiontriggered after toggleMenuItemView method called
     */
    subscribeRemoveOption(): Observable<any> {
        return this.removeItem.asObservable();
    }

    /**
     * @name setLanguage(isShow)
     * @description used to set selected language
     */
    setLanguage() {
        this.setSelectedLanguage.next({});
    }
}
