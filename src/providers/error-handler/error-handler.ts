// angular imports section
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// ionic imports section
import { ToastController, ModalController } from 'ionic-angular';

@Injectable()
export class ErrorHandlerProvider {

    constructor(public http: HttpClient, public toastCtrl: ToastController, public modalController: ModalController) {
    }

    /**
     * @method handleError()
     * @param error error message
     * @param type type of the error
     * @description used to handle all kind of errors
     */
    handleError(error, type) {
        
        let customClass: string = '';

        if (type === 'error') {
            customClass = 'error';
        } else if (type === 'success') {
            customClass = 'success';
        } else if (type === 'warning') {
            customClass = 'warning';
        } else if (type === 'info') {
            customClass = 'info';
        }

        let data = {'class': customClass, 'errorMessage': error}

        let modalPage = this.modalController.create('ErrorMessageSnackBarPage', data);
        modalPage.onDidDismiss(data => {
        });
        modalPage.present();
    }

}
