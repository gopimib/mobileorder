// angular imports section
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

// cofig file imports section
import { AppConfig } from '../../app/app.config';

@Injectable()
export class AppServiceProvider {

    public checkoption = { 'category': '', 'index': -1 };
    public tableId: string = '';
    public addMoreItems: any;
    public menuAndItemDetails: any;
    public recallData: any;
    public defaultSettings: any = '';
    public httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };
    public deviceId: string = '';
    public floorData: string = null;
    public baseUrl: string = '';
    public modifiersData: any = '';
    public openTable: any;
    public headCount: any;
    public selectedLanguage: any;

    constructor(public http: HttpClient) {
        this.baseUrl = AppConfig.baseUrl;
    }

    /**
     * @method doLogin()
     * @description used to login based on  credential data
     */
    doLogin(data) {
        return this.http.post(this.baseUrl + 'UserAuthentication/api/Authenticate/UserValidation', data, this.httpOptions);
    }

    /**
     * @method getLanguageBasedText()
     * @description used to get current language using get method
     */
    getLanguageBasedText(language) {

        let data = { 'TranslationInputText': '' };
        data.TranslationInputText = language === 'english' ? 'en-us' : 'zh-cn';
        return this.http.post(this.baseUrl + 'LanguageTranslator/api/Translator/LanguageTranslator', data, this.httpOptions); 
    }

    /**
     * @method callPrintApi()
     * @param printReqData 
     * @description used to print the 
     **/
    callPrintApi(printReqData) {
        return this.http.post(this.baseUrl + 'Print/api/Print/PrintData', printReqData, this.httpOptions);
    }

    /**
     * @method getFloorStructure()
     * @description used to get current floor structure using post method
     */
    getFloorStructure(data) {
        return this.http.post(this.baseUrl + 'Svg/api/FloorPlan/GetSvg', data, this.httpOptions);
    }

    /**
     * @method getImage()
     * @description used to get image
     */
    getImage(imageId) {
        return this.http.get(this.baseUrl + 'ImageDetailsUILayer/api/Image/GetImage?itemID=' + imageId);
    }

    /**
     * @method getDataAndChoice()
     * @description used to get menu and items using post method
     */
    getDataAndChoice(data) {

        data['LanguageId'] = localStorage.getItem('lang') === 'English' ? 'en-us' : 'zh-cn';
        data['EnterpriseId'] = '30';
        return this.http.post(this.baseUrl + 'Menu/api/Menu/MenuListItems', data, this.httpOptions);
    }

    /**
     * @method getOpenOrdersDetails()
     * @description used to get already opened tables records using post method
     */
    getOpenOrdersDetails() {
        let data = { 'profitcenterid': localStorage.getItem('profitCenterId') };
        return this.http.post(this.baseUrl + 'OpenOrders/api/check/OpenOrders', data, this.httpOptions);
    }

    /**
     * @method recallCheck()
     * @param data object contains table id
     * @description used to get items of selected table using post method
     */
    recallCheck(data) {
        data['EmpId'] = localStorage.getItem('empId');
        return this.http.post(this.baseUrl + 'RecallCheck/api/RecallCheck/GetRecallCheck', data, this.httpOptions);
    }

    /**
     * @method doOpencheck()
     * @param data object contains table id
     * @description used to open a new check for selected table using post method
     */
    doOpencheck(data) {
        data['EmpId'] = localStorage.getItem('empId');
        return this.http.post(this.baseUrl + 'OpenCheck/api/Check/OpenCheck', data, this.httpOptions);
    }

    /**
     * @method doModifyCheck()
     * @param data object contains table id, check type, check number and items
     * @description used to add a new items for selected table using post method
     */
    doModifyCheck(data) {
        data['EmpId'] = localStorage.getItem('empId');
        return this.http.post(this.baseUrl + 'ModifyCheck/api/Check/ModifyCheck', data, this.httpOptions);
    }

    /**
     * @method closeCheck()
     * @param data object contains table id
     * @description used to close a check for selected table using post method
     */
    closeCheck(data) {
        data['EmpId'] = localStorage.getItem('empId');
        return this.http.post(this.baseUrl + 'CloseCheck/api/Check/CloseCheck', data, this.httpOptions);
    }

    /**
     * @method doGenerateAlipayQrCode()
     * @param data object
     * @description used to send api call to alipay for generating qr code string for selected table using post method
     */
    doGenerateAlipayQrCode(data) {
        return this.http.post(this.baseUrl + 'EFMO/api/Home/RetriveQRTransactionCode', data, this.httpOptions);
    }

    /**
     * @method checkPullAPI()
     * @param data object
     * @description used to check alipay notfication api if its come this api will pull that response
     */
    checkPullAPI(data) {
        return this.http.post(this.baseUrl + 'AlipayTransaction/api/PAYMENTDETAILS/GetPaymentDetailsByRequestID', data, this.httpOptions);
    }

    /**
     * @method closeAlipayPaymentCheck()
     * @param data object contains check details like amount, tip, check number, table id and etc
     * @description used to check alipay notfication api, if its come this api will pull that response
     */
    closeAlipayPaymentCheck(data) {
        return this.http.post(this.baseUrl + 'CloseCheckExternal/api/externalpay/CloseCheck', data, this.httpOptions);
    }

    /**
     * @method doSplitCheck()
     * @param data object contains split check details
     * @description used to split check based on user input
     */
    doSplitCheck(data) {
        return this.http.post(this.baseUrl + 'SplitCheck/api/Check/InsertSplitCheck', data, this.httpOptions);
    }

    /**
     * @method doSplitCheck()
     * @param data object contains check number
     * @description used to check selectd table check is already splited or not
     */
    getSplitCheck(data) {
        return this.http.post(this.baseUrl + 'SplitCheck/api/Check/GetSplitCheck', data, this.httpOptions);
    }

    /**
     * @method closeSplitCheck()
     * @param data object contains check details like amount, tip, check number, table id and etc
     * @description used to close entire splitted check
     */
    closeSplitCheck(data) {
        return this.http.post(this.baseUrl + 'SplitCheck/api/Check/UpdateSplitCheck', data, this.httpOptions);
    }

    /**
     * @method getRecentClosedChecks(data)
     * @param data object contains check number, employee number and profit center id
     * @description used to get recent closed checks
     */
    getRecentClosedChecks(data) {
        return this.http.post(this.baseUrl + '/PrintReceipt/api/PrintReceipt/GetDetails', data, this.httpOptions);
    }

    /**
     * @method sendEmail(data)
     * @param data object contains check number, tender amount, closed date, receipt text and to address
     * @description used to get recent closed checks
     */
    sendEmail(data) {
        return this.http.post(this.baseUrl + 'SendMail/api/ProcessEmail/Send', data, this.httpOptions);
    }

    /**
     * @method getOpenOrdersStatus(data)
     * @param data object contains profit center id
     * @description used to get open orders
     */
    getOpenOrdersStatus(data) {
        data['EmpId'] = localStorage.getItem('empId');
        return this.http.post(this.baseUrl + 'OrderStatus/api/OrderStatus/Status', data, this.httpOptions);
    }

    /**
     * @method getModifiersList(data)
     * @param data object contains profit center id
     * @description used to get modifers
     */
    getModifiersList() {
        let data = { 'profitcenterid': localStorage.getItem('profitCenterId') };
        data['LanguageId'] = localStorage.getItem('lang') === 'English' ? 'en-us' : 'zh-cn';
        data['EnterpriseId'] = '30';
        return this.http.post(this.baseUrl + 'Menu/api/Modifier/GetModifier', data, this.httpOptions);
    }

    /**
     * @method setCheckOptionDetail()
     * @param option object contains category and index
     * @description used to set selected check option name and index
     */
    setCheckOptionDetail(option) {
        this.checkoption.category = option.category;
        this.checkoption.index = option.index;
    }

    /**
     * @method getCheckOptionDetail()
     * @description used to get selected check option name and index
     */
    getCheckOptionDetail() {
        return this.checkoption;
    }

    /**
     * @method setSelectedTableId()
     * @param tableId table number
     * @description used to set selected table number
     */
    setSelectedTableId(tableId) {
        this.tableId = tableId;
    }

    /**
     * @method getSelectedTableId()
     * @description used to get selected table number
     */
    getSelectedTableId() {
        return this.tableId;
    }

    /**
     * @method setAddMoreItemsValues()
     * @param tableId selected table number
     * @param checkNumber selectd check number
     * @description used to set selected table number and check number in recall check method
     */
    setAddMoreItemsValues(tableId, checkNumber) {
        this.addMoreItems = { 'tableId': tableId, 'checkNumber': checkNumber };
    }

    /**
     * @method getAddMoreItemsValues()
     * @description used to get selected table number while modifing the items for selected check
     */
    getAddMoreItemsValues() {
        return this.addMoreItems;
    }

    /**
     * @method setMenuDetails()
     * @param data contains entire menu and items api response
     * @description used to set entire menu and items in one variable
     */
    setMenuDetails(data: any) {
        this.menuAndItemDetails = data;
    }

    /**
     * @method setMenuDetails()
     * @description used to get entire menu and items
     */
    getMenuDetails() {
        return this.menuAndItemDetails;
    }

    /**
     * @method setDeviceId()
     * @param deviceId current device id
     * @description used to set current device id for alipay payment purpose
     */
    setDeviceId(deviceId: string) {
        this.deviceId = deviceId;
    }

    /**
     * @method getDeviceId()
     * @description used to get current device
     */
    getDeviceId(): string {
        return this.deviceId;
    }

    /**
     * @method setFloorData()
     * @param data svg data
     * @description used to set svg data for future refrence
     */
    setFloorData(data) {
        this.floorData = data;
    }

    /**
     * @method getFloorData()
     * @description used to get stored svg data
     */
    getFloorData() {
        return this.floorData;
    }

    /**
     * @method setRecallData()
     * @param data contains item response of selected table
     * @description used to set item response for future refrence
     */
    setRecallData(data) {
        this.recallData = data;
    }

    /**
     * @method getRecallData()
     * @description used to get selected table item response
     */
    getRecallData() {
        return this.recallData;
    }

    /**
     * @method setOpenCheckDetails()
     * @param data contains entire response of getOpenOrdersDetails method api
     * @description used to set open order response
     */
    setOpenCheckDetails(data) {
        this.openTable = data;
    }

    /**
     * @method getOpenCheckDetails()
     * @description used to get open order response
     */
    getOpenCheckDetails() {
        return this.openTable;
    }

    /**
     * @method setModifersData()
     * @param data contains entire response of modifiers list
     * @description used to set open order response
     */
    setModifersData(data) {
        this.modifiersData = data;
    }

    /**
    * @method getModifersData()
     * @description used to get modifiers list response
     */
    getModifersData() {
        return this.modifiersData
    }

    /**
     * @method setModifersData()
     * @param data contains head count number
     * @description used to set head count for the check
     */
    setHeadCountOnTable(data: number) {
        this.headCount = data;
    }

    /**
     * @method getModifersData()
     * @description used to get added head count for the check
     */
    getHeadCountOnTable() {
        return this.headCount
    }

    /**
     * @method setDefaultSettings()
     * @param data contains currency symbol
     * @description used to set default setting value from login screen
     */
    setDefaultSettings(data) {
        this.defaultSettings = data;
    }

    /**
     * @method getDefaultSettings()
     * @description used to get default settings
     */
    getDefaultSettings() {
        return this.defaultSettings
    }

    /**
     * @method setSelectedLanguageTexts()
     * @param data contains language texts
     * @description used to set selected language static texts
     */
    setSelectedLanguageTexts(data) {
        this.selectedLanguage = data;
    }

    /**
     * @method getSelectedLanguageTexts()
     * @description used to get selected language static texts
     */
    getSelectedLanguageTexts() {
        return this.selectedLanguage
    }
}
