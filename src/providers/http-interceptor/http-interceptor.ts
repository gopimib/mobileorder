// angular imports section
import { HttpClient, HttpInterceptor, HttpEvent, HttpErrorResponse, HttpResponse, HttpRequest, HttpHandler } from '@angular/common/http';
import { Injectable, Inject, InjectionToken } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TimeoutError } from 'rxjs/util/TimeoutError';
import 'rxjs/add/operator/timeout';

// providers imports section
import { ErrorHandlerProvider } from '../error-handler/error-handler';
import { AppBroadcastProvider } from '../app-broadcast/app-broadcast';

@Injectable()
export class HttpInterceptorProvider implements HttpInterceptor {

    constructor(public errorHandlerServie: ErrorHandlerProvider, private appBroadcastProvider: AppBroadcastProvider) { }

    /**
     * @method intercept()
     * @param req requested url
     * @param next method call back
     * @description it will return error if the api call does not return response with in 10 seconds  
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const timeout = 180000;
        req.headers.delete('timeout');
        
        return next.handle(req).timeout(timeout).catch((err: HttpErrorResponse) => {
            this.appBroadcastProvider.display(false);
            this.errorHandlerServie.handleError('Something Unexpected Happened. Please Try Again Later....', 'error');
            return Observable.of(new HttpResponse({ body: { 'error': true } }));
        });
    }
}
