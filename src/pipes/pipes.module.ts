// angular imports section
import { NgModule } from '@angular/core';

// pipes imports section
import { MyPipe } from './my/my';

@NgModule({
	declarations: [MyPipe],
	imports: [],
	exports: [MyPipe]
})
export class PipesModule { }
