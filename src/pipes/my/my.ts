// angular imports section
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
    name: 'myPipe',
})

export class MyPipe implements PipeTransform {
    constructor(private sanitizer: DomSanitizer) {
    }

    /** 
     * @method transform(content)
     * @param content - svg data
     * @description used to convert string into trusted html dom elements
     */
    transform(content) {
        return this.sanitizer.bypassSecurityTrustHtml(content);
    }
}